![](X:\Document\Project\BIG\Zhihu-asdasd1dsadsa\Article\Figures\NoResponse.png)

# Mathematica 12.1 启动缓慢/无响应的解决方案

> 先随便说两句，之后再补。

## 为什么启动会卡？

因为初始化内核时出于某种原因（比如对于 `"Update" -> Automatic` 的包进行自动更新），调用了

```mathematica
PacletManager`Package`PacletInstallSubmit 
```

，其中又要以

```mathematica
UpdatePacletSites -> True
```

选项调用

```mathematica
PacletFindRemote
```

，从而反复（我尚不确定，如果真是这样的话那很蠢）更新站点信息。考虑到国内连接 Wolfram 服务器现状，这会非常慢，在我这里需要 30 秒以上完成。



哪怕你挂梯子更新一次之后，如果超过一段时间（0.2/1/3/7 天，分多种情况）没更新，这个情况就又会出现。

## 怎么解决？

* 改设置：

	```mathematica
	CurrentValue[$FrontEnd, "AllowDownloads"] = False
	```

	或

	```mathematica
	CurrentValue[$FrontEnd, "AllowDocumentationUpdates"] = False;CurrentValue[$FrontEnd, "AllowDataUpdates"] = False
	```

* 搞个位于美国的梯子，每次用的时候都挂上

* 修改 `PacletManager` 源码

## 如何修改源码？

在

```mathematica
FileNameJoin@{DirectoryName@FindFile@"PacletManager`", "Manager.m"}
```

文件中，找到 `pacletInstallPreamble` 的定义，在定义中找到 `PacletFindRemote` ，将其 `UpdatePacletSites` 选项设为 `False` 。



这么做完全阻止了包信息的自动更新，从而用户在有需要时需要手动更新。有空我再提供更好的方案。

## 问题还是没有解决怎么办？

这个情况的出现确实还有其他的可能原因，比如在 Windows 10 v1903+Mathematica 11.3+ 我观测到了几例类似问题（在处理帮助文档/动态/幻灯片时卡住），我曾解决过一些（MMASE上也有一些相应的解决方案），不过这个算是亚洲地区MMA用户的特色问题所以专门提一下。有问题可以留言问，说不定我见过。