![使用Wolfram语言内置功能实现简单的服务器后端](Figures/v2-c347ca7124ef3ed23ba0f5d960308c51_1200x500-1acbbce2-79df-4ff1-bded-c82e98462101.jpg)

# 使用Wolfram语言内置功能实现简单的服务器后端

由于WolframCloud在国内的连接现状惨不忍睹，所以创建一个自己的WolframCloud是有作用的。这里并不实现一个完整的WolframCloud，而是一个更简单的例子。更重要的是，我是在用它来介绍HTTP响应相关的内置功能，主要起教育普及作用。

[https://github.com/asdasd1dsadsa/SimpleWolframServer](https://github.com/asdasd1dsadsa/SimpleWolframServer)

> 注意：如果没有合适的许可证，用户不能将自己的服务提供给其他人、从而使Wolfram产品内容暴露给无许可证者。

---

## 使用SocketListen来监听端口并进行ZeroMQ会话

* 用法:

```mathematica
SocketListen[host, handlerFunction]
SocketListen[{ip,port}, handlerFunction]
```

执行一个SocketListen指令后会创建一个SocketListener对象，对来自ip的对port的消息进行异步监听。

* 例:

```mathematica
SocketListen["0.0.0.0:8080",(
    WriteString[#SourceSocket,
        ExportString[requestHandler@ImportString[#Data, "HTTPRequest"], "HTTPResponse"]
    ];
    Close@#SourceSocket
)&]
```

## 使用HTTPResponse和GenerateHTTPResponse对请求进行响应

* 用法

```mathematica
HTTPResponse[body, assoc]
```

assoc中是包含"StatusCode"等键的关联，这个关联不需要写的很全，HTTPResponse按照一个默认策略来自动补全缺失的规则。

```mathematica
GenerateHTTPResponse[anything, request]
```

GenerateHTTPResponse基于一系列自动策略把anything 构成一个HTTP响应，有时会用到request中的信息，用不到的时候可以略去request。 

* 例

这里给出一些对上述requestHandler的定义。

1. 无论收到什么请求都返回默认的404响应。如果受到的不是有效请求，则返回502。

```mathematica
requestHandler[request_HTTPRequest] := GenerateHTTPResponse@HTTPErrorResponse@404
requestHandler[$Failed] := GenerateHTTPResponse@HTTPErrorResponse@502
```

2\. 一个最简单的FormPage

```mathematica
requestHandler[request_HTTPRequest] := GenerateHTTPResponse[fp, makeFormRules@request]
fp = FormPage[
    "expr" -> ToExpression,
    #expr &
];
```

这里用到了一个makeFormRules是因为FormFunction、FormPage将用户点击“Submit”按钮所产生的HTTPRequest对象的FormRules属性作为用户输入来处理，但ImportString\[\#, "HTTPRequest"\] &给出的HTTPRequest并不具有这一属性。Wolfram语言之所以没有使它是这样，我估计可能是想给我们一个自己定制的空间。

要处理上述例子，makeFormRules只需考虑ContentType 是application/x\-www\-form\-urlencoded的情况 ：

```mathematica
makeFormRules[request_HTTPRequest] := MapAt[
    URLQueryDecode@StringTrim@FromCharacterCode[#, "UTF-8"] &,
    request,
    {2, "Body"}
]
```

做到这一步，你就可以通过访问自己的服务器来计算任何表达式了（包括Quit\[\]）。作为一个FormPage的拓展示例，我们可以让表达式的输入框更大一些：

```mathematica
FormPage[
    "expr" -> <|
        "Interpreter" -> ToExpression,
        "Label" -> "Expression",
        "Control" -> Function@InputField[##, FieldSize -> {Large, 10}]
    |>,
    #expr &
]
```

详见FormObject文档。

## 使用URLDispatcher来分派URL

URLDispatcher服务于GenerateHTTPResponse，它对GenerateHTTPResponse的第二参数（request） 中的"Path" 进行某种适用于URL的子字符串匹配。

* 用法

```mathematica
URLDispatcher@{strPattern1 -> anything1, strPattern2 -> anything2, ...}
```

其中anything可以是另一个URLDispatcher，这个内部的URLDispatcher会对上级Dispatcher匹配剩下的路径进行再分派。 

值得注意的是，空字符串模式将匹配所有字符串，按照子串匹配的默认策略，它将匹配最长的子串，但URLDispatcher的策略不同，它从StartOfString开始一直到下一个/字符就停下。所以常常用空模式来截去一个“目录名”（见下例）。 

* 例

```mathematica
requestHandler[request_HTTPRequest] := GenerateHTTPResponse[
    $mainURLDispatcher
, makeFormRules@request]
$mainURLDispatcher = URLDispatcher@{
    "" -> URLDispatcher@{
        StartOfString ~~ ""|"/" ~~ EndOfString -> "Hello.\nHere is the root path",
        "/eval" -> fp
    }
}
```

在此例中，对http://yourhost/进行访问将看到欢迎字样，访问 http://yourhost/eval 则看到之前定义的FormPage——最外层URLDispatcher中的空模式截取了yourhost而将/eval留给了内层的dispatcher。 

## 提供文件访问服务

之前例子中的FormPage生成的HTML中引用了一些css,js文件，如果用浏览器访问（而不是不需要渲染页面的URLRead），那么对js、css文件的访问仍被定向到WolframCloud官网从而还是很慢。我们可以将这些文件的访问定向到自己服务器所指定的目录。

首先对GenerateHTTPResponse生成的HTML进行字符串替换来改变资源请求的路径。比如：

```mathematica
resourceLocalize = MapAt[
    StringReplace[{
        Alternatives@@$cloudResourcePath ~~ FileName_ -> "/Resources/" ~~ FileName,
        "http://www.wolframcdn.com/consent/cookie-consent.php" -> ""
    }] @ ExportString[#, "Byte"] &
, 1]
```

然后实现一个传输文件内容的服务：

```mathematica
getResource[path_String] := With[
    {
        localpath = Sequence@@FileNameSplit@path
    },
    If[FileExistsQ@localpath,
        HTTPResponse[
            Import[localpath, "String"],
                <|"ContentType" -> extToMIME@FileFormat@localpath|>
        ],
        HTTPErrorResponse[404]
    ]
]
$mainURLDispatcher = URLDispatcher@{""->#}&@URLDispatcher@{
    anyRulesElse,
    path__ :> GetResource@path
}
```

这里的文件访问服务匹配所有剩下的路径，应当放在最后以免屏蔽住了其他服务（没错，URLDispatcher也是优先执行靠前的规则的）。

extToMIME的内容文章里就不写了，可以找个字典去实现。不要用自带的Interpreter\["MIMETypeString"\]，有坑。

## 返回详细的请求、响应内容

参考HTTPRequestData、ResponseForm。

## 结语

要提供更广泛的服务 ，就避不开自己实现各种各样的功能了。对于网页服务，可以使用Templating\`HTMLTemplate以及XHTML相关支持来开发。对于API开发，APIFunction应该是够用的。

虽然我刚接触这方面，知道的不多。但我仍估计，是不必对Wolfram语言进行服务器开发的能力进行怀疑的，因为我知道用它可以创造出WolframCloud和WolframAlpha。
