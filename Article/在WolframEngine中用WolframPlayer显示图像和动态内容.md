![在 Wolfram Engine 中用 Wolfram Player 显示图像和动态内容](Figures/v2-937c5d7416592967b04d4c46f69b380b_1200x500-1364e921-78bd-408f-985f-e25b68e44bce.jpg)

# 在WolframEngine中用WolframPlayer显示图像和动态内容

笔记本相关功能不被 Wolfram Engine 支持，所以不能将图形对象等直接导出为 Notebook 或 CDF 文件，但它仍有构建框符的能力。我们可以自己构建 Notebook 表达式并导出为 Notebook 或 CDF 文件。

Wolfram Engine 不支持 SystemOpen ，但我们可以使用 RunProcess 系列功能调用 Wolfram Player 打开导出的 CDF 文件。

作为一个示范，我在 init\.m 中定义 WolframPlayer 函数来调用 Wolfram Player 打开动态/图像内容，并修改 \$Post 来让它作用于每一个动态/图像输出。

```mathematica
BeginPackage["CommandLineUtilities`Display`"];

WolframPlayer;

Begin["`Private`"];

$TemporaryOutput = FileNameJoin@{$TemporaryDirectory, "WolframKernelOutput"};
If[!DirectoryQ@#, CreateDirectory@#] &@$TemporaryOutput;
WriteToShell[str_] := (
	If[Head@$ShellProcess =!= ProcessObject, $ShellProcess = StartProcess@$SystemShell];
	WriteLine[$ShellProcess, str];
	str
)

MakeNotebook[box_] := Notebook[{Cell@BoxData@box}, WindowSize -> All]

WolframPlayer[expr_, box_] := (
	$WolframPlayerProcess = If[# === {}, StartProcess@"WolframPlayer", None] &@SystemProcesses@"WolframPlayer";
	StringJoin[
		"WolframPlayer ",
		Export[FileNameJoin@{$TemporaryOutput, CreateUUID["CDFOutput-"]<>".cdf"}, MakeNotebook@box, "CDF"]
	] //WriteToShell;
	ExportString[OutputForm@expr, "Text"]
)

$Epilog := (
	KillProcess/@{$ShellProcess, $WolframPlayerProcess};
	Quiet@DeleteFile@FileNames[__, $TemporaryOutput];
	If[FindFile["end`"] =!= $Failed, << "end`"] (* Original definition. You can read it with OwnValues. *)
)

End[];

EndPackage[];

(*$DisplayFunction = WolframPlayer[#, ToBoxes@#]&;*)
$Post = With[{box = ToBoxes@#},
	If[FreeQ[DynamicBox|DynamicModuleBox|GraphicsBox|Graphics3DBox]@box,
			#,
			WolframPlayer[#, box]
	]
]&;
```

如此一来，输出动态/图形时就会自动使用上面定义的 WolframPlayer 函数调用 Wolfram Player 来显示了。

> 初次使用图形/动态指令时时要加载 Process 相关功能，并启动一个 shell 进程和 WolframPlayer 进程，会耗上一些时间。不过这些指令不会在内核启动时就执行，不会让内核启动过程耗时很久。

如果你不想使用 WolframPlayer ，你可以用类似的方法将图像导出 PNG 图像，然后调用你想要的查看器去打开。
