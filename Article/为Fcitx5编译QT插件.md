# 为 fcitx5 编译 Qt 插件

（据我所知的）Mathematica 目前没有任何版本内置有 fcitx 或 fcitx5 的 Qt 插件，这导致我们无法在 Mathematica 中唤起基于 fcitx 的输入法，如我使用的 fcitx5-rime 。

解决方案是，自行编译一份符合 Mathematica 所用 Qt 的版本的 fcitx 插件，放入 `$InstallationDirectory/SystemFiles/Libraries/Linux-x86-64/Qt/plugins/platforminputcontexts/` 文件夹。

参考资料：

1. https://tieba.baidu.com/p/6115426645
2. https://www.csslayer.info/wordpress/fcitx-dev/a-case-study-how-to-compile-a-fcitx-platforminputcontext-plugin-for-a-proprietary-software-that-uses-qt-5/
3. https://fcitx-im.org/wiki/Compiling_fcitx5

Mathematica 的网上已有的相关资料较老，比如可能是旧版的 Mathematica 和 fcitx4 。本文则是针对较新的 Mathematica 12.2.0 和 fcitx5 。

### 确认需要的 Qt 版本

在文件夹 `$InstallationDirectory/SystemFiles/Libraries/Linux-x86-64/Qt/lib/` 中找到 Qt 的库文件，用 `strings` 查看其版本信息：

```bash
strings libQt5Core.so.5 |grep "Qt 5"
```

我这里是

```
Qt 5.11.0 (x86_64-little_endian-lp64 shared (dynamic) release build; by GCC 6.3.1 20170216 (Red Hat 6.3.1-3))
This is the QtCore library version Qt 5.11.0 (x86_64-little_endian-lp64 shared (dynamic) release build; by GCC 6.3.1 20170216 (Red Hat 6.3.1-3))
If that is not possible, in Qt 5 you must at least reimplement
```

可知是 `5.11.0` 。

## 使用编译好的文件

如果你确定自己的 Mathematica 版本或 Qt 版本与一些库文件发布者的相符，那么可以考虑直接下载来用。

1. [Mathematica 11.3 on manjaro kde](https://tieba.baidu.com/p/6115426645)

2. [Mathematica 12.2.0 on gentoo x11](https://gitee.com/asdasd1dsadsa/ZhiHu-asdasd1dsadsa/raw/master/Resources/fcitx5plugin/Mathematica%2012.2.0%20on%20gentoo%20x11/libfcitx5platforminputcontextplugin.so)

3. [Qt 5.11.0 on gentoo x11(for Mathematica 12.2.0)](https://gitee.com/asdasd1dsadsa/ZhiHu-asdasd1dsadsa/raw/master/Resources/fcitx5plugin/Qt%205.11.0%20on%20gentoo%20x11/libfcitx5platforminputcontextplugin.so)
4. [Qt 5.15.2 on gentoo x11(for Mathematica 13.1.0)](https://gitee.com/asdasd1dsadsa/ZhiHu-asdasd1dsadsa/raw/master/Resources/fcitx5plugin/Qt%205.15.2%20on%20gentoo%20x11/libfcitx5platforminputcontextplugin.so)

## 自行编译

### 配置 Qt 环境

由于我用的 gentoo 系统的包管理器已不储存有这个版本，且我系统内已经有一个较新版本的 Qt 了，我将使用系统包管理器以外的方法配置环境。一般来说有两种方法：

1. 使用官方的 Qt Installer：https://www.qt.io/download-qt-installer
2. 从 https://download.qt.io/new_archive/qt/5.11/5.11.0/ 下载

我采用官方的 Qt Installer 并使用 Qt Creator ——要多下载一些东西但是将来能够免于自己配置环境：

1. 下载安装器并运行
2. 注册帐号并登录
3. 选择条款和许可证选项
4. 选择信息收集条款
5. 更新完元信息后，选择安装目录，并选择「Custom installation」
6. 在组件选择页面中，在右边的筛选器处选择「Archive」，再次更新元信息后才能看到旧版本 Qt 组件的选项
7. 我选择了「Qt -> Qt 5.11.0」中的「Sources」和「Desktop gcc 64-bit」组件。「Qt Creator 7.0.0」会自动选中，待会我们也会用到它。
8. 选择许可协议并安装

### 配置 fcitx5-qt 的依赖

参考官方指南：https://fcitx-im.org/wiki/Compiling_fcitx5

#### 安装 xcb-imdkit

作为 gentoo 用户，我已经有一份了，就算没有也可以用包管理器安装：

```bash
sudo emerge -a xcb-imdkit
```

如果你的包管理器没有这个包，也可以依照官方指南自己手动编译并安装。

```bash
git clone https://github.com/fcitx/xcb-imdkit.git --depth=1
cd xcb-imdkit
cmake .
make
sudo make install
```

这需要使用 `cmake` 和编译器，如果你系统中没有 `cmake` ，可以用包管理器装一份。Qt Installer 为你下载的「Desktop gcc 64-bit」已带有必要的组件，配置一下环境变量就好了。

#### 安装 fcitx5

作为 gentoo 用户，我已经有一份了，就算没有也可以用包管理器安装。具体操作参考上一节以及官方指南。

### 编译 fcitx5-qt

先克隆仓库

```bash
git clone https://github.com/fcitx/fcitx5-qt.git --depth=1
cd fcitx5-qt
```

但这里我不用 cmake 编译。因为我系统中原先已有更新版本的 Qt ，不改脚本的话默认会用新版的。这里直接使用刚才安装的 Qt Creator 就能省去些功夫：

1. 在 `fcitx5-qt` 仓库文件夹中的 `CMakeLists.txt` 中，把 `option(ENABLE_QT4 "Enable Qt 4" On)` 改为 `option(ENABLE_QT4 "Enable Qt 4" Off)` 。
1. 打开 Qt Creator，点击「Open Project」，打开刚才修改后的 `CMakeLists.txt` 。
3. 首次打开会让你配置项目。这里选中你想要的版本就好，我这里是「Desktop Qt 5.11.0 GCC 64bit」。在子配置中，只选择一个（如「Release」），并把路径改为 `fcitx5-qt` 自身，而不是专门创建一个文件夹存放构建结果。最后点击「Configure Project」创建配置。
4. 在菜单栏中选择「构建 -> 构建项目 fcitx5-qt」。（由于配置已经执行了 `cmake` 生成了 `CMakeCache.txt` ，现在也可以用命令行手动构建）

此时你应该可以在 `fcitx5-qt/qt5/platforminputcontext/` 中找到 `libfcitx5platforminputcontextplugin.so` 。

#### 链接到 Mathematica 自带的库

效仿 [这篇文章](https://www.csslayer.info/wordpress/fcitx-dev/a-case-study-how-to-compile-a-fcitx-platforminputcontext-plugin-for-a-proprietary-software-that-uses-qt-5/) 的做法，用 Mathematica 自带的 Qt 库文件来链接，以防 Mathematica 对库的魔改。

> 然而似乎并没有这样做的必要，跟用 Qt 内置库编译的没发现用起来有什么区别。

打开 `CMakeCache.txt` ，找到并设置

```
CMAKE_VERBOSE_MAKEFILE:BOOL=ON
```

然后执行

```bash
cmake --build . --target clean
cmake --build . --target all > build.log
grep libfcitx5platforminputcontextplugin.so build.log
```

在它所输出的形如

```bash
/usr/bin/g++ -fPIC -Wall -Wextra  -g  -Wl,--no-undefined -Wl,--as-needed -shared  -o qt5/platforminputcontext/libfcitx5platforminputcontextplugin.so qt5/platforminputcontext/CMakeFiles/fcitx5platforminputcontextplugin.dir/fcitx5platforminputcontextplugin_autogen/mocs_compilation.cpp.o qt5/platforminputcontext/CMakeFiles/fcitx5platforminputcontextplugin.dir/qfcitxplatforminputcontext.cpp.o qt5/platforminputcontext/CMakeFiles/fcitx5platforminputcontextplugin.dir/fcitxcandidatewindow.cpp.o qt5/platforminputcontext/CMakeFiles/fcitx5platforminputcontextplugin.dir/fcitxtheme.cpp.o qt5/platforminputcontext/CMakeFiles/fcitx5platforminputcontextplugin.dir/font.cpp.o qt5/platforminputcontext/CMakeFiles/fcitx5platforminputcontextplugin.dir/qtkey.cpp.o qt5/platforminputcontext/CMakeFiles/fcitx5platforminputcontextplugin.dir/main.cpp.o  -Wl,-rpath,/some/path/to/qt/Qt/5.11.0/gcc_64/lib:/some/path/to/repo/fcitx5-qt/qt5/dbusaddons:  /some/path/to/qt/Qt/5.11.0/gcc_64/lib/libQt5Gui.so.5.11.0  /usr/lib64/libxcb.so  qt5/dbusaddons/libFcitx5Qt5DBusAddons.so.5.0.11  /usr/lib64/libxkbcommon.so  /some/path/to/qt/Qt/5.11.0/gcc_64/lib/libQt5DBus.so.5.11.0  /some/path/to/qt/Qt/5.11.0/gcc_64/lib/libQt5Core.so.5.11.0
```

的编译命令中，尽可能地把涉及的库文件的路径改为 Mathematica 的 `$InstallationDirectory/SystemFiles/Libraries/Linux-x86-64/Qt/lib/` 中的，形如：

```
/usr/bin/g++ -fPIC -Wall -Wextra  -g  -Wl,--no-undefined -Wl,--as-needed -shared  -o qt5/platforminputcontext/libfcitx5platforminputcontextplugin.so qt5/platforminputcontext/CMakeFiles/fcitx5platforminputcontextplugin.dir/fcitx5platforminputcontextplugin_autogen/mocs_compilation.cpp.o qt5/platforminputcontext/CMakeFiles/fcitx5platforminputcontextplugin.dir/qfcitxplatforminputcontext.cpp.o qt5/platforminputcontext/CMakeFiles/fcitx5platforminputcontextplugin.dir/fcitxcandidatewindow.cpp.o qt5/platforminputcontext/CMakeFiles/fcitx5platforminputcontextplugin.dir/fcitxtheme.cpp.o qt5/platforminputcontext/CMakeFiles/fcitx5platforminputcontextplugin.dir/font.cpp.o qt5/platforminputcontext/CMakeFiles/fcitx5platforminputcontextplugin.dir/qtkey.cpp.o qt5/platforminputcontext/CMakeFiles/fcitx5platforminputcontextplugin.dir/main.cpp.o  -Wl,-rpath,/some/path/to/qt/Qt/5.11.0/gcc_64/lib:/some/path/to/repo/fcitx5-qt/qt5/dbusaddons:  $InstallationDirectory/SystemFiles/Libraries/Linux-x86-64/Qt/lib/libQt5Gui.so.5  /usr/lib64/libxcb.so  qt5/dbusaddons/libFcitx5Qt5DBusAddons.so.5.0.11  /usr/lib64/libxkbcommon.so $InstallationDirectory/SystemFiles/Libraries/Linux-x86-64/Qt/lib/libQt5DBus.so.5  $InstallationDirectory/SystemFiles/Libraries/Linux-x86-64/Qt/lib/libQt5Core.so.5
```

然后执行即可生成 `qt5/platforminputcontext/libfcitx5platforminputcontextplugin.so` 。
