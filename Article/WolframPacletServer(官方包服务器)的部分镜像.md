![Wolfram Paclet Server\(官方包服务器\)的镜像](Figures/v2-c243fac94a814ed24711977195656f62_1200x500-7ec85117-51b8-45c8-9bd2-b306ef2a03e7.jpg)

# WolframPacletServer(官方包服务器)的镜像



由于国内与Wolfram官方的包服务器的连接惨不忍睹，故这里设下镜像作为替代的包服务器，只需一条指令即可启用。

[WolframPaclet/WolframPacletGeneral](https://gitee.com/wolframpaclet/WolframPacletGeneral/)

在码云储存了几乎所有包的最新版和一些常用包的旧版本，并利用 CloudFlare Workers 代理了官方包服务器，原则上可以通过本项目访问所有官方包！
![Wolfram Engine命令行环境效果展示](Figures/v2-efd84d32c0cdd7b85b1beca50de379d2_r-20202a24-2681-443a-9356-f5db465c4700.jpg)
如果有其他人想维护此项目（更新包和包信息），可以联系我，我会提供该帐号并介绍管理工具的使用。维护项目所需的命令都已封装，可以说是可以“一键维护”的了。这个管理工具包也允许我们轻松地从零开始重建镜像。
![管理工具包的帮助文档](Figures/v2-40deedde7af358f5eb3b31bba76b0786_r-a7aad27d-215c-471a-abb0-55bb7abed3bc.jpg)

