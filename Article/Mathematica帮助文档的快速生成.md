# Mathematica帮助文档的快速生成

最近在写一个帮助文档，便开发了一个帮助文档生成器。目前版本0\.2\.1，支持符号、教程和指南、概述页面的生成。

---

## 需求的背景（无用，不看也罢）

对于帮助文档的生成，官方提供的解决方案是Wolfram Workbench，它带有 DocumentationTools 、 DocumentationBuild 等程序包，用于生成文档。

我（仅仅是）尝试着用过它（所以以下陈述只是管中窥豹），但是它似乎是依照符号的 usage 消息来生成文档的，而具体的文档编辑还是需要手动从框符层面编辑， DocumentationTools 面板也没提供什么对文档编辑的便利性有很大帮助的按钮。

由于我觉得 Workbench 开发环境不如笔记本，以及没有那个心思去读那个 967K 的 DocumentationTools\.m 更别说 50多M的 DocumentationBuild\.paclet 。我放弃了这条方案。

除 Workbench 外，我也看了一些第三方包：

* ApplicationMaker 程序包带有 DocumentationMake 功能。通过 NewGuide 等函数生成一个帮助文档框架。这些函数只接受 appName, appDir 这样的基本参数，定制化程度很低。* 
ForScience 程序包中的 PlotUtils/DocumentationBuild 接受一些参数来生成笔记本表达式。* 
BTools 程序包提供函数来生成一个模版笔记本，这应该是目前最完善的生成器了。用户可利用该笔记本的工具栏编辑模版并转化文档。不过可能是因为版本原因，我在尝试该转化时前端崩溃了。

我所开发的文档编辑器创建了一个可计算的样式表和包含一系列实用工具的程序包，专注于用户输入到帮助文档的快捷转化。

---

## 功能展示
![左：用户编辑 → 右：计算结果](Figures/v2-1abc4ff898e4692578f885a57931ce09_r-d9477167-9297-40ed-a23a-440e02126561.jpg)
图中左边的笔记本使用我定义的SymbolPage样式表，其中几乎所有单元都可以计算，计算之后会根据用户在这些单元中输入的内容来创建文档。

在这些单元中，用户可以只输入字符串（快捷），也可以输入框符（定制）。

在DocumentationCreator程序包中，也定义了如LinkedExpression这样的函数来帮助用户快捷地创建框符。

---

## 使用方法

此包仍在开发，暂存于码云上，可用此指令安装。

```
PacletInstall["DocumentationCreator", Site -> "https://gitee.com/wolframpaclet/DocumentationGenerator/raw/master/"]
```

我当然是按照自己的需求去更新项目，不打算很快就建立很系统的程序包，所以如果读者忍不住要改进它也并不奇怪，我只希望读者如果有好的改进可以提起PULL REQUEST。

目前此程序包还没有文档教程，了解框符的用户可以直接阅读源代码。对于普通用户，我在这里暂时先简单介绍一下用法，更好的办法是研究研究此程序包自带的 LinkedExpression\.nb （即题图）。

安装此包后，你能在样式表中找到DocumentationCreator中的SymbolPage
![](Figures/v2-65357eafe27ee0eade2c970564bf2b0e_r-3bfd879a-823f-4d43-be30-fc1dfae4e3e7.jpg)
选择此样式表后，你可以使用 **Alt\+1~7快捷键** 分别创建7种样式的单元，其中有一些单元可以 **使用Tab和Shift\+Tab键转换** 为其他没有设定快捷键的样式的单元。

1.  **AnchorBarGrid** : 生成顶栏，用户应输入字符串（具体会显示在哪可以自己试试）
2.  **PrimaryExamplesSection** : 范例节，使用Tab可转化为范例小节 **ExampleSection** （使用Backspace还原），使用Shift\+Tab在范例节与信息节 **NotesSection** 间转换。它们都可接受字符串或框符，但范例节与范例小节还可以接受字符串（或框符）的二维列表，具体用法可参考题图。
3.  **Notes** : 信息文本，字符串或框符
4.  **ExampleText** : 例子文本，字符串或框符
5.  **ExampleDelimiter** : 例子分隔符，什么也不用输入
6.  **SeeAlso** : “参见”栏目，通过Tab和Shift\+Tab与“更多指南 **MoreAbout** ”、“教程 **Tutorials** ”栏目相转化。指南和教程栏目接受n\*2矩阵，此矩阵应当匹配模式 $ { \rm \left\{ \left\{ someBoxOrString1\_, link1\_String \right\}.. \right\} } $ 。参见栏目接受符号构成的列矩阵\(n\*1\)。
7.  **RuleSetter** : 此单元没有什么渲染作用，但用于设置笔记本的 TaggingRules 属性，接受规则列表。用法参考题图。

写好这些以后计算整个笔记本即可完成转化。转化后自动切换样式表为 Reference\-zh\.nb ，但不会为笔记本设置Saveable等属性。用户如有需要应手动设置。
