![为Wolfram Engine配置VSCode（Windows）](Figures/v2-466ee356c7a3809ab8db918fab2902e6_1200x500-6c5b230e-6dbb-43da-8dc6-a1a662ce6482.jpg)

# 为WolframEngine配置VSCode（Windows）

因为我只尝试过Windows系统，所以内容是针对Windows系统写的。

## 第一步：下载安装Wolfram引擎

国内连接Wolfram官网进行下载的状况堪忧，可以从这里找到Windows和Linux版本的云盘下载链接：

[https://tiebamma.github.io/InstallTutorial/](https://tiebamma.github.io/InstallTutorial/)

安装过程略。

## 第二步：注册WolframID以及申请Wolfram引擎许可证

申请链接：

[Wolfram Engine for Developers License](https://www.wolfram.com/engine/free-license/)

<u>注：现在此页面已重新开放。</u>

## 第三步：用WolframID和密码激活Wolfram引擎

联网运行Wolfram引擎目录下的wolfram\.exe，启动成功后会要求用户输入WolframID，回车后再要求输入密码。输入的密码不会显示在屏幕上，输完回车就行了。

尚不明确：有时wolfram\.exe并未要求输入WolframID而是要求激活码和密钥，你可以改为尝试运行wolframscript来激活；有时明明已经激活过了但启动内核却仍要求激活，可能是因为你启动了（超出免费许可证允许的）多个内核，需要额外的账户去激活才能使用。

## 第四步：配置VSCode的wolfram文件关联

按【Ctrl\+,】打开设置，点击右上角的【打开设置\(json\)】按钮打开settings\.json。找到

```json
"files.associations"
```

插入

```
"*.wls": "wolfram"
```

记得加逗号。

如果找不到现成的就直接插入新的

```
"files.associations": {"*.wls": "wolfram"}
```

## 第五步：配置VSCode自带的CodeRunner插件的设置

在settings\.json中插入或修改

```
"code-runner.executorMap": {"wolfram": "wolfram -script"}
```

如果没有在环境变量中添加Wolfram引擎所在目录，则必须指定wolfram\.exe的完整路径。

另一种可行的选项是

```
"code-runner.executorMap": {"wolfram": "wolframscript -file"}
```

但这种方法不如上一种好，WolframScript本身其实也是去调用WolframKernel进行计算，而WolframScript本身似乎（从目前测试看来）不支持中文路径。两者在其他方面也有些不同的地方。

## 第六步：安装语法高亮插件

在扩展商店中搜索【shigma\.vscode\-wl】并安装相应插件。根据我使用体验，此插件尚不完善，但目前没有更好的。

作者的开发笔记：[https://zhuanlan.zhihu.com/p/52722078](https://zhuanlan.zhihu.com/p/52722078)

## 第七步：安装插件【lsp\-wl\.lsp\-wl\-client】

此插件提供【提纲】、【自动完成】、【语法检查】、【帮助文档浮窗】功能。

作者写的中文介绍：[https://zhuanlan.zhihu.com/p/74921013](https://zhuanlan.zhihu.com/p/74921013)

## 第八步：为插件【lsp\-wl】配置Wolfram引擎

【lsp\-wl】插件需要添加其在Wolfram Kernel部分的组件才是完整的。

现在，用Wolfram引擎联网执行

```
PacletInstall["CodeParser"]
PacletInstall["CodeInspector"]
```

来安装这个组件所要调用的一些包。如果安装耗时太久或者网络错误，可以先配置镜像站 [https://gitee.com/wolframpaclet/WolframPacletGeneral](https://gitee.com/wolframpaclet/WolframPacletGeneral) 再下载。

如果你希望插件自动更新，则还需执行（不装也行）

```
PacletInstall["https://github.com/WolframResearch/GitLink/releases/download/v0.1.1/GitLink-2018.07.20.01.paclet"]
```

使用Git来clone这个Repository

[https://github.com/kenkangxgwe/lsp-wl](https://github.com/kenkangxgwe/lsp-wl)

你也可以在网页上点击下载，但那样的话这个插件就不能借助 GitLink 自动更新。

> 如果你使用Mathematica 11\.2而不是更高版本或Wolfram Engine，你需要clone该Github仓库的develop分支。

然后，用Wolfram引擎执行刚才从GitHub下载的目录中的【init\.m】

## 第九步：配置插件【lsp\-wl】的VSCode设置

打开VSCode设置。找到【Wolfram Language Server: WLServerPath】，输入GitHub仓库【lsp\-wl】的本地路径；在【Wolfram Language Server: Wolfram Path】中设置好wolfram\.exe的路径。

## 第十步：重启VSCode

---

## 关于编码问题

一般习惯使用UTF\-8编码，如果各方面的数据编码对不上就会乱码。建议进行以下设置：

1. 将系统编码改为UTF8。按Windows10的情况讲解，就是先打开【区域与语言】的系统设置，再点击【管理语言设置】，再点击【管理】选项卡中的【更改系统区域设置】，再勾选【使用Unicode UTF\-8提供全球语言支持】，最后重启。
2. 将VSCode默认文件编码设为UTF\-8。见【File: Encodings】。
3. 将Wolfram系统的默认编码设为UTF\-8。找到你所用Wolfram系统的Kernel的init\.m，加入

```
$CharacterEncoding = "UTF8"
```

## 关于前端问题

Wolfram引擎本身不支持前端功能。从而，输出需要使用Print来使其显示，互动可视化应当由图形/动画的文件输出代替（或参考 [https://zhuanlan.zhihu.com/p/134171410](https://zhuanlan.zhihu.com/p/134171410) 一文），```NotebookDirectory[]```应以```Directory[]```、```DirectoryName@$InputFileName```、```DirectoryName@First@$ScriptCommandLine```、```"\."```等代替。

---

如有错误遗漏希望各位及时指出。如果有人开发了好的插件希望多宣传宣传。
