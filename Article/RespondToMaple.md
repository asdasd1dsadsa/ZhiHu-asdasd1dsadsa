# How Mathematica Responds to How Maple Compares to Mathematica

本文对  [Maple与Mathematica孰美](https://www.maplesoft.com/products/maple/compare/HowMapleComparestoMathematica.pdf) 中涉及 Mathematica 的部分予以回应。总得来说，这文中成文太早，实际上对当今的 Maple 和 Mathematica 的参考意义都有限。

为了撰文效率，主要参考的是 [MapleSoftChina](https://www.zhihu.com/org/maplesoftchina) 的翻译版。

## 关于「标准的数学记号」

> | Maple使用标准的数学符号，圆括号表示函数 f(x) ,               | Mathematica中使用方括号 f[x]                                 |
> | ------------------------------------------------------------ | ------------------------------------------------------------ |
> | Maple使用中的函数使用标准的表达，首字母小写 log(x),sin(x),cos(x) | Mathematica中使用首字母大写Log[x]，Sin[x], Cos[x]            |
> | Maple中等号表示为“=”                                         | Mathematica中，等号用于变量赋值，双等号表示等于，因此表达会报错 |
> | Maple中自动使用二维的分数和指数表达，例如，当输入“/”时，Maple插入水平分数线，然后你的输入就会出现在分母的位置 | Mathematica中分数和指数不是自动应用格式，使用功能键和标准键组合输入（如Ctrl+/输入“/”）或先输入Mathematica表达式 再用菜单操作将其转化为传统形式。 |
> | Maple通过默认面板使用标准数学格式。                          | Mathematica中，面板输入命令。必须使用菜单操作将命令转化为标准的数学形式。 |

### 1

首先， Mathematica **同样支持**符合现行数学习惯的记号作为输入和输出，称为 `TraditionalForm` ，但不是默认设置（默认的是 `StandardForm` ）。这是因为 **Wolfram Language 将自己定位为编程语言**，力求无歧义，也算是保有一种改进数学记号的态度。

**现行数学记号约定的歧义**的一个例子是 `f(g+h)` 。它可以被理解为映射 `f` 作用于 `g+h` 上，也可以理解为乘法 $f\times(g+h)$ 。

### 2

关于分数线的快捷键的问题，**你完全可以设定** Mathematica ，使其用 `/` 来插入二维分数线，只不过这不是默认设置。这个默认设置以及等号的问题的原因与之前所说的相同：Wolfram Language 定位自己为一个编程语言，那么 Mathematica 自然是优先支持用得最多的形式。

### 3

> Mathematica中，输入“2+2<Enter>”将会把光标移到下一行，不会作任何计算。需要执行计算，必须敲击<Shift>+<Enter>。这种非标准的输入需要用户改变他们的习惯。

首先，**你同样可以设定** Mathematica ，使其通过 Enter 来执行计算，而且按下小键盘 Enter 的默认行为就是执行计算。

这样设计的原因在于，Mathematica **希望你能够方便的插入换行符**。这既**符合编程语言的一般习惯**，也符合 Mathematica 的另一个定位：**可计算文档**的编辑器。可计算文档是 WRI 比较引以为豪的一个设计，后来被 JupyterNotebook 借鉴并流行。

相比于 Mathematica 这个可计算的“文档”，用 Enter 执行计算的应用，比较像“计算器”。

## 关于排版

> Mathematica中，即使使用传统样式，仍然与教科书的标准不同。如变量名没有使用斜体。

首先，你**同样可以设定** Mathematica 使用符合教科书标准的设定。许多内置样式表都有 `DisplayFormula` 样式，如果设定个好的字体就更好看了。变量名不使用斜体，是因为使用者使用了不使用斜体的样式。

此外，**Mathematica 是真的有被用于排版教科书的**，他们官网的资料库里有。也有一些第三方的，不过我忘记地址了。知乎上也有比较精美的排版案例。

> - Maple的 中很容易将文字和数学合并到同一个句子中。甚至可以将计算结果放到句子中间，因此当计算结果改变时，这些语句可以自动更新。如下图，当改变函数定义并重新运行文档时，得到新的结果。
>
> ![img](https://pic4.zhimg.com/80/v2-e96c0be5c6c5d8c22ea570e2678e127f_720w.jpg)
>
> - Mathematica中不能用这种方式实现文本和数学结果的合并。你可以在一个单元中显示文本和静态公式，但不能显示计算结果。如果计算结果改变了，必须手动编辑进行修改。

Mathematica **同样可以**同时将文本、代码、公式写到同一行、同一单元。关于计算结果的自动更新，可以使用 `Dynamic` 系列功能。即便手动更改也绝不是徒手重输，可以适时使用 `Evaluate In Place` 菜单指令。

## 关于撤销

> Mathematica中你只能撤消最近的一个基本编辑操作（如剪切，复制，输入文本）。你不能重做未完成的操作，或撤消最近的多个操作。如果需要撤消多个操作，唯一的办法是返回到文件的早期版本（保存过的文件）。

看来这篇文章成文较早，那时应该确实不支持无限步撤销。但**在现在这个时间点看来，无限步撤销已经是很早版本就加入的功能了**。

不过 **Mathematica 的撤销确实有 BUG** ，一直持续没修。不过遇到的倒不是很多，用习惯了就知道怎么避坑了，还行。

## 关于“可点击数学”

这个我懒得说，因为 Mathematica 确实不是默认提供这些功能（有一个建议栏的功能，新手可以用，老手一般是禁用掉），但不意味着这不可能。

Mathematica 的互动能力实际上很强。**你在 Mathematica 中见到的一切窗口实际上都是一个笔记本**，也就是说你可以通过编程来实现相同的功能。不过确实**对新手而言学习成本较高，而且有时性能不够看**。

## 关于数学计算功能和性能

这些功能在几年来两个软件应该都有不少增强，也许应该重新比拼一下看看，结果不好说。我的见闻是两者互有长短，而不是文中附图所显示的 Maple 完爆另一方。我不了解 Maple 就不多说了。

如果挑选对 Wolfram Language 有利的测试，也许也能塑造出一个反向完爆的印象。比较老的版本的对比也要多看看几个来源。我所见过的有这个：[Comparison of mathematical pro-
grams for data analysis](https://web.archive.org/web/20131001000000*/http://www.scientificweb.com/ncrunch/ncrunch5.pdf)

> **微分几何**：Maple的微分几何支持覆盖了广泛的领域，从喷气计算到广义相对论背后的数学。它也包含一系列的微分几何教程，包括初级和高级专题。Mathematica不提供任何的函数支持微分几何。

虽然 Mathematica 基本上没有内置支持，但第三方包 xAct 用户蛮多的。

## 关于编程范式

> Mathematica也支持不同的编程风格，但是主要是函数化的方法。这种方法难以读，写和调试。

确实普遍认为，函数式比过程式编程难学，但也许这难度中有很多成分来自于用户的知识结构。大多数人学习的第一门语言都是过程式或者面向对象的语言，而很少有 LISP 这样的。可能在 LISP 用户看来，Mathematica 非常容易。

我读、写自己的 Mathematica 代码显著快于其他语言，因为代码短。至于调试，这方面确实有些负面评价。但绝大多数情况下，**当用户觉得调试困难的时候，往往是他们没有了解如何使用一个笔记本来调试程序，没有按照语言特性去解耦合程序，从而使得调试困难**。

此外，为 `PrintDefinitions` 设定一个快捷键将很有帮助。

> 语法错误是编程中常见的错误。编译器或翻译器的任务是帮助程序员查找错误并尽可能简单地改正。下面的一个简单程序，因缺少分号，Maple会很快定位到这个语法错误并将光标移到这个位置，而Mathematica不能得到所期望的答案，也没有任何提示信息。

**Mathematica 确实不会把光标定位到语法错误的位置**。但 Wolfram Language 的语法非常简单，无非就是括号逗号和运算符（还有一些行首专用运算符如 `<<` ），没有任何关键字或特殊用法。**文中提到的 `If[]*Return[]` 实际上是逻辑错误而非语法错误**，因为 `If` 和用户的自定函数并没有根本的区别，而且主要的内置函数都有现成的 `SyntaxInformation` 定义，使得在你输入了不正确的参数数目等情况下**对错误的代码上色**。复杂函数的逻辑错误则一般由错误处理机制来做，过去主要是 `Message` `Catch` ，近年来多了不少新支持。

## 关于并行

> Mathematica限制核的数量为4，除非购买额外的许可。

这个确实。盗版无限，个人用的各种正版限制4，人称正版受害者。

## 关于数值模型

> Maple的数值模型源自IEEE/754浮点标准……Mathematica使用的数值模型派生自一个“有效位运算”的东西，不是国际标准，而且细节也未公布。其他系统写的算法在Mathematica中实现时常会得到不同结果，而且差别是不可预测的。虽然每种系统在浮点运算上有其固有的优势和弱点，Maple的模型的优势是易于理解，且经过了可识别的问题验证。相反，Mathematica的专有模型意味着错误的结果不是总能预测或检测。
>
> ![](https://pic2.zhimg.com/80/v2-a4c2ef94cbc6882e7577bae05483c9bd_720w.jpg)

首先，Wolfram Language **同样支持** IEEE/754 浮点数，这类浮点数在 Wolfram Language 中称为「**机器精度**」。文中提到的「有效位运算」是 Wolfram Language 中的另一套数值模型，称为「任意精度」，引入任意精度的数值模型是为了满足符号计算中常出现的大数运算的需要。也就是说， **Wolfram Language 是同时支持两套体系，而不是因为支持一套自己的而放弃了另一套通用的**。

图中右边的 Mathematica 样例，使用了 `SetAccuracy` **强制**产生了任意精度浮点数，并用于与左边 Maple 所使用的 IEEE/754 浮点数比较，这没有意义。完全可以把第二行代码替换为 `s[0]=3/10//N` 来使用机器精度。而且无论是机器精度还是任意精度，在目前的 Wolfram Language 版本中都已**无法复现**图中的问题。

## 关于代码生成和外部语言接口

> Mathematica可以导出C或Fortran代码，但不能直接使用，因为它不是标准的C或Fortran代码。

如果这里说的是 `CForm` 和 `FortranForm` ，那么没错，**这两个功能不能生成立刻可用的代码，而主要是用于转换简单数学表达式**。类似的但“more functional but less portable”的转换器还存在于 `Export` 中。

对于 C 语言，Wolfram Language 有一个专门的 `CompilationTarget -> "C"` 。Wolfram Language 中还存在专门的 `CCodeGenerator` 和 `SymbolicC` 。出现比较早的还有 Java Link 和 .net Link 。

近年来， Wolfram Language 投入大量资源在与其他语言的交互上。你可以通过 `WSTP + Client Library` `LibraryLink` `mcc` `ExternalEvaluate` 等方式使用外部语言或在外部语言中调用 Wolfram Kernel 。最新的（其实也有三年了） `FunctionCompile` 则能编译生成 LLVM IR 。

> Maple紧密集成在MATLAB中……Mathematica不支持任何内建联系。第三方工具提供调用MATLAB函数或生成MATLAB代码功能，但不被Wolfram Research支持。代码生成工具有十多年没有更新了。不支持相互的通讯和代码转换。

这个确实，第三方的 `MATLink` 包太老了。

> Mathematica提供工具导出3D对象成CAD格式，但不提供两种产品中的主动连接。没有办法从CAD图形中获取参数，也不可能将新的参数值直接推送到CAD设计中。

据我所知（不可靠），这一点到现在应该也确实如文中所说。似乎一般 Wolfram Language 比较愿意直接将开源库接入、继承进来，而对于非开源的外部程序，一般支持仅限于格式等开放性较好的交互渠道。（用得特别多的可能称为例外，如 `ChromeDriver` `ExcelLink` ）

## 关于源代码查看

> Mathematica中所有的内置库由Mathematica编程语言开发，但对用户是隐藏的。源代码存储在专有文件格式.mx文件中。用户不能查看这些代码，也不可能使用Mathematica调试器逐步运行它们。因为代码是不可见的，也不可能自定义Mathematica库程序。
> 



> Mathematica中所有的内置库由Mathematica编程语言开发

说得不严谨。部分功能应该是直接由底层语言（如C）实现的，此外还有用Link调外部库的做法。

> 源代码存储在专有文件格式.mx文件中。用户不能查看这些代码

**mx的解密方法确实目前仍不清楚**，但不是不可查看的。**一切由 Wolfram Language 进行的定义，都可由 DownValues 等函数查看**。

部分实现为 `DownCodes` 的定义不可查看，但往往实际上仍然是调用了可查看定义的函数。比如作为符号化简功能入口的符号 `Simplify` 只有 `DownCodes` 而没有 `DownValues` ，但其主要功能所对应的函数都可以在 ``` Simplify` ``` 等上下文中找到，它们大多是可查看定义的。

此外，`Trace` 的 `TraceInternal` 选项也很有用。

> 不可能自定义Mathematica库程序

随手翻下我在 Mathematica 话题下的知乎回答都能找到一堆修改了内置函数的：

* https://www.zhihu.com/question/536333961/answer/2518275057

* https://www.zhihu.com/question/40605388/answer/2124353487

* https://www.zhihu.com/question/481140724/answer/2084059444
