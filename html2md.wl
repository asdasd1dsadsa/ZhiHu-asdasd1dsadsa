(* ::Package:: *)

(* ::Title:: *)
(*ZhiHuToMarkdown*)


(* ::Chapter:: *)
(**)


BeginPackage["ZhiHuToMarkdown`"]


Clear@"`*"


ImportZhihu
ZhihuToMarkdown
ExportMarkdown


BackupZhihu


$PackageConstant


Begin["`Private`"]


Clear@"`*"


(* ::Chapter:: *)
(*\:5e38\:91cf*)


$PackageConstant = <|
	"EscapeCharacters" -> Characters@"\\$*`>_{}[]()#+-.!^",
	"FigureDirectoryName" -> "Figures",
	"FigureLocalization" -> True,
	"MarkdownOnline" -> True
|>;


(* ::Chapter:: *)
(*\:5bfc\:5165*)


ImportZhihu[url_String] := Import[url, "XMLObject"]

ImportZhihu[questionID_Integer, answerID_Integer] :=
	ImportZhihu@StringTemplate["https://www.zhihu.com/question/``/answer/``"][IntegerString@questionID, IntegerString@answerID]
	
ImportZhihu[id_Integer] := ImportZhihu@StringTemplate["https://zhuanlan.zhihu.com/p/``"]@IntegerString@id


(* ::Chapter:: *)
(*\:8f6c\:6362\:89c4\:5219*)


(* ::Section:: *)
(*\:57fa\:672c\:89c4\:5219*)


(* ::Text:: *)
(*\:7531\:4e8e\:5185\:7f6eXML\:89e3\:6790\:4f3c\:4e4e\:5b58\:5728\:9519\:8bef\:884c\:4e3a\:ff0c\:6545\:4f5c\:9632\:8303*)
(*\:9519\:8bef\:4e00\:4f8b\:ff1ahttps://www.zhihu.com/question/28875649/answer/1123476915*)


ZhihuToMarkdown[_] := ""


(* ::Subsection:: *)
(*\:4f5c\:7528\:4e8e\:5bcc\:6587\:672c\:5185\:5bb9*)


ZhihuToMarkdown[xml:XMLObject["Document"][___]] := FirstCase[
	xml,
	XMLElement["div", {"class" -> "RichContent-inner"|"RichText ztext Post-RichText"}, content_] :> ZhihuToMarkdown@content,
	Failure[
		"Unable to find any target post. Apply \"ZhihuToMarkdown\" directly on the rich-content XMLObject may help.",
		<|"FailedFunction" -> FirstCase|>
	]
, Infinity]


ZhihuToMarkdown@XMLElement["span", {___, "class" -> "RichText ztext CopyrightRichText-richText", ___}, {}] := ""


(* ::Subsection:: *)
(*\:5206\:914d\:5230\:5185\:90e8*)


ZhihuToMarkdown[list_List] := ZhihuToMarkdown/@list //StringJoin


ZhihuToMarkdown@XMLElement["span", {}, _]


ZhihuToMarkdown@XMLElement["div", {}, _]


(* ::Subsection:: *)
(*\:5b57\:7b26\:4e32\:8f6c\:4e49*)


ZhihuToMarkdown[str_String] := StringReplace[# -> "\\"<># & /@ $PackageConstant["EscapeCharacters"]]@str


(* ::Text:: *)
(*StringTemplate["%``"]@IntegerString[#, 10, 2] & /@ Range@99 // StringJoin // URLDecode*)
(*% // ToCharacterCode*)


(* ::Section:: *)
(*\:683c\:5f0f*)


(* ::Subsection:: *)
(*\:5b57\:4f53*)


(* ::Subsubsection:: *)
(*\:7c97\:4f53*)


ZhihuToMarkdown@XMLElement["b", {}, bold_] := " **"<>ZhihuToMarkdown@bold<>"** "


(* ::Subsubsection:: *)
(*\:659c\:4f53*)


ZhihuToMarkdown@XMLElement["i", {}, italic_] := " *"<>ZhihuToMarkdown@italic<>"* "


(* ::Subsubsection:: *)
(*\:4e0b\:5212\:7ebf*)


ZhihuToMarkdown@XMLElement["u", {}, underline_] := "<u>"<>ZhihuToMarkdown@underline<>"</u>"


(* ::Section:: *)
(*\:5185\:5bb9\:5bf9\:8c61\:7684\:8f6c\:6362\:89c4\:5219*)


(* ::Subsection:: *)
(*\:8d85\:94fe\:63a5*)


anchorEncode[str_String] := StringReplace["_" -> "-"]@str

parseLink[zhihuURL_] := StringReplace[{
	StartOfString~~"http://"|"https://"|""~~"link.zhihu.com/?target="~~urlEncoded___~~EndOfString :> URLDecode@urlEncoded,
	StartOfString~~"#"~~anchor___~~EndOfString :> anchorEncode@anchor
}]@zhihuURL


(* ::Subsubsection:: *)
(*URL\:94fe\:63a5*)


ZhihuToMarkdown@XMLElement["a", {___, "href" -> url_, ___}, content_] := FirstCase[
	content,
	XMLElement["span", {___, "class" -> "LinkCard-title", ___}, {text_String}] :>
		StringTemplate["\n[``](``)\n"][text, parseLink@url],
	StringTemplate["[`1`](`1`)"]@parseLink@url
, Infinity]


(* ::Subsubsection:: *)
(*\:53c2\:8003\:94fe\:63a5*)


(* ::Text:: *)
(*ZhihuToMarkdown@XMLElement["sup", _, {XMLElement["a", {___, "href" -> anchor_?(StringMatchQ["#ref_" ~~ __]), ___}, {text_String}]}] :=*)
(*   	StringTemplate["[``][^``]"][text, anchor~StringDrop~1]*)


ZhihuToMarkdown@XMLElement["sup", _, {XMLElement["a", {___, "href" -> anchor_?(StringMatchQ["#ref_"~~__]), ___}, {text_String}]}] :=
	"[^"<>StringRiffle[StringExtract[anchor, "_" -> 2;;], "."]<>"]"


(* ::Subsection:: *)
(*\:516c\:5f0f*)


decodeZhihuFormula[zhihuFormula_] := StringReplace[
	StartOfString~~"http://"|"https://"|""~~"www.zhihu.com/equation?tex="~~formulaEncoded___~~EndOfString :> URLDecode@formulaEncoded
]@zhihuFormula


(* ::Subsubsection:: *)
(*\:884c\:5185\:516c\:5f0f*)


ZhihuToMarkdown@XMLElement["img", {___, "src" -> inlineFormula_, ___}, {}] := "$ "<>decodeZhihuFormula@inlineFormula<>" $"


(* ::Subsubsection:: *)
(*\:516c\:5f0f*)


ZhihuToMarkdown@XMLElement["p", {}, {XMLElement["img", "img", {___, "src" -> formula_, ___}, {}, {}]}] := "$$\n"<>decodeZhihuFormula@formula<>"\n$$"


(* ::Subsection:: *)
(*\:89c6\:9891*)


(* ::Subsection:: *)
(*\:4ee3\:7801*)


(* ::Subsubsection:: *)
(*\:884c\:5185\:4ee3\:7801*)


ZhihuToMarkdown@XMLElement["code", {}, inlineCode_] := ZhihuToMarkdown@inlineCode


(* ::Subsubsection:: *)
(*\:4ee3\:7801\:5757*)


parseHighlight@XMLElement["div", {"class" -> "highlight"}, {XMLElement["pre", {}, {XMLElement["code", {"class" -> language_}, list_]}]}] :=
	"\n```"<>StringReplace["language-text"|"language-" :> ""]@language<>"\n"<>parseHighlight@list<>"\n```\n"


parseHighlight@XMLElement[_, Except@{"class" -> "highlight"}, content_] := parseHighlight@content


parseHighlight[list_List] := parseHighlight/@list //StringJoin


parseHighlight[str_String] := str


ZhihuToMarkdown[highlight:XMLElement["div", {"class" -> "highlight"}, _]] := parseHighlight@highlight


(* ::Subsection:: *)
(*\:56fe\:7247*)


FigureLocalize[img_?(StringContainsQ["zhimg.com"])] := If[TrueQ@$PackageConstant["FigureLocalization"],
	(
		Which[
			DirectoryQ@#, Null,
			!FileExistsQ@#, CreateDirectory@#,
			True, DeleteFile@#(* somehow dangerous *);CreateDirectory@#
		];
		StringRiffle[FileNameSplit[#][[-2;;]], "/"] & @@URLDownload[{img}, #]
	),
	img
]&@$PackageConstant["FigureDirectoryName"]


ZhihuToMarkdown@XMLElement["figure", _, {
	___,
	XMLElement["img", {___, "data-original"(*|"data-default-watermark-src"*) -> img_, ___}, {}],
	XMLElement["figcaption", {}, figcaption_],
	___
}] := "\n!["<>ZhihuToMarkdown@figcaption<>"]("<>FigureLocalize@img<>")\n"


ZhihuToMarkdown@XMLElement["figure", _, {
	___,
	XMLElement["img", {___, "data-original"(*|"data-default-watermark-src"*) -> img_, ___}, {}],
	Except@XMLElement["figcaption", __]|PatternSequence[]
}] := "\n![]("<>FigureLocalize@img<>")\n"


(* ::Subsection:: *)
(*\:6c34\:5e73\:7ebf*)


ZhihuToMarkdown@XMLElement["hr", {}, {}] := "\n---\n"


(* ::Subsection:: *)
(*\:4e0d\:6253\:7b97\:652f\:6301\:7684\:5143\:7d20*)


(* ::Text:: *)
(*\:597d\:7269\:63a8\:8350*)


(* ::Text:: *)
(*\:9644\:4ef6*)


(* ::Section:: *)
(*\:5c42\:7ea7\:5bf9\:8c61\:7684\:8f6c\:6362\:89c4\:5219*)


(* ::Subsection:: *)
(*\:6807\:9898*)


(* ::Subsubsection:: *)
(*\:4e8c\:7ea7\:6807\:9898*)


ZhihuToMarkdown@XMLElement["h2", {}, header2_]  := "\n## "<>ZhihuToMarkdown@header2<>"\n"


(* ::Subsection:: *)
(*\:6bb5\:843d*)


ZhihuToMarkdown@XMLElement["p", {}, paragraph_] := "\n"<>ZhihuToMarkdown@paragraph<>"\n"


(* ::Subsection:: *)
(*\:5f15\:7528*)


ZhihuToMarkdown@XMLElement["blockquote", {}, quote_List] := "\n> "<>ZhihuToMarkdown@quote<>"\n"


(* ::Subsection:: *)
(*\:5217\:8868*)


(* ::Subsubsection:: *)
(*\:6709\:5e8f\:5217\:8868*)


ZhihuToMarkdown@XMLElement["ol", {}, list:{XMLElement["li", {}, _]..}] :=
	"\n"<>StringRiffle[MapIndexed[IntegerString[#2]<>". "<>ZhihuToMarkdown@Last@# &, list], "\n"]<>"\n"


(* ::Subsubsection:: *)
(*\:65e0\:5e8f\:5217\:8868*)


ZhihuToMarkdown@XMLElement["ul", {}, list:{XMLElement["li", {}, _]..}] :=
	"\n* "<>StringRiffle[ZhihuToMarkdown@Last@# & /@ list, "* \n"]<>"\n"


(* ::Subsubsection:: *)
(*\:53c2\:8003\:6587\:732e\:5217\:8868*)


ZhihuToMarkdown@XMLElement["ol", {"class" -> "ReferenceList"}, list:{XMLElement["li", _, {XMLElement["a", _, _], XMLElement["span", _, {_String}]}]..}] :=
	"\n"<>StringRiffle[StringTemplate["[^``]: ``"][StringRiffle[StringExtract["id"~Replace~#[[2]], "_" -> 2;;], "."], #[[3, 2, 3, 1]]]& /@ list, "\n\n"]<>"\n"


(* ::Chapter:: *)
(*\:61d2\:4eba\:5de5\:5177*)


titleFunction = If[TrueQ@$PackageConstant["MarkdownOnline"],
	StringReplace[" " -> ""],
	Identity
]


BackupZhihu[any__] := With[{xml = ImportZhihu@any},
	Export[
		#1<>".md",
		#2<>"# "<>#1<>"\n"<>ZhihuToMarkdown@xml
	, "Text"]&[
		FirstCase[xml, XMLElement["h1", {"class" -> "QuestionHeader-title"|"Post-Title"}, {title_String}] :> titleFunction@title, StringRiffle[URLEncode@*TextString/@{any}, "-"], Infinity],
		FirstCase[xml, XMLElement["img", {OrderlessPatternSequence["class" -> "TitleImage", "src" -> img_, "alt" -> figcaption_String]}, {}] :> "!["<>ZhihuToMarkdown@figcaption<>"]("<>FigureLocalize@img<>")\n\n", "", Infinity]
	]
]


(* ::Subtitle:: *)
(**)


End[]


EndPackage[]
