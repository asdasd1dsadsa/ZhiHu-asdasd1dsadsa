# Mathematica做理论推导有哪些好用的技巧？

此回答写给稍微具有一些使用经验的用户，如果你是新手但想要获得理论推导相关的使用经验，我认为你或许从贴吧精品区-常见问题栏目中能学到更多。原因不仅仅在于（对新手来说）本文可能过于难懂，也在于避免犯错通常比新的技巧要重要得多。

我会逐渐完善此回答。最近更新：2020\.10.29

## 使用模式匹配进行代数式的整理

这本来应该是每个 Mathematica 使用者都该掌握的，但是很多把它当计算器用的人就是没去学模式匹配，只知道用内置的 `Collect` 啥的，不知道如何去实现自己定制的需求。

曾有群友希望对一个由余弦函数线性组合而来的表达式进行项的归并，将同频率的项放在一块儿。我给出一个参考的解法是：

```mathematica
target = a1 Cos[f*t+c] + b1 Cos[2(f*t+c)] + b2 Cos[2f*t+3c]
timeFreeQ[expr_] := FreeQ[expr, t, -1, Heads -> False]
result = GatherBy[
	List@@MapAll[Expand, target],
	Replace[_Symbol Cos[freq_ t + ___?timeFreeQ] :> freq]@*TrigReduce
]
Plus@@@result
Defer@*Plus@@%
```

手动化简这一操作本质上也是规则的匹配替换过程，原则上你总是可以用程序表达它，从而使其可以自动化完成。

拓展阅读小例子：[Mathematica Stack Exchange - Replacing value by another value](https://mathematica.stackexchange.com/questions/212488/replacing-value-by-another-value/212491)

## 在局部使一个符号没有直接值\(OwnValues\)

许多时候，你先对符号 `x` 进行了赋值，但之后你又希望临时对 `x` 进行符号计算再代入这个值，比如求导。这时你可能想这样做：

```mathematica
x = 2;
Clear@x
result = D[x^2, x];
x = 2;
result
```

正确的做法是放弃 `D` 而改用 `Derivative` ：

```mathematica
x = 2;
Derivative[1][#^2 &]@x
```

但不是每个符号计算函数都有不用符号的等价写法（比如 Collect ），所以你往往还是得清除定义。实际上你完全可以使用 Block ：

```mathematica
x = 2;
Block[{x}, Collect[a x + b y + c x, x]]
```

此外，如果你从来都不在符号计算中给符号赋值，那么自然也就不会有上述需求。此时如果你偶尔需要赋值，可以用 `ReplaceAll` 来代替

```mathematica
Collect[a x + b y + c x, x] /. x -> 2
```

## 在定义中使保持符号不被代入数值

和上一节的需求类似，这次我们尝试建立这样一个函数 `f` ，我们期望它相当于已知函数 $g(x)$ 的导函数 $g'(x)$ ：

```mathematica
Clear[f,g]
g[x_] := x^2
f[x_] := D[g[x],x]
```

这一写法是 **错误** 的。当你计算 `f[1]` 时，它按照定义被替换为 `D[g[1], 1]` 乃至 `D[1, 1]` ，而 `D` 的用法要求其第二参数是符号，于是报错。

一种可行的做法是使用上面提过的、不依赖于符号的 `Derivative` ：

```mathematica
Clear[f,g]
g[x_] := x^2
f[x_] := Derivative[1][g][x]
(* 也可以用 Derivative 的运算符写作： *)
f[x_] := g'[x]
```

还有一种做法：

```mathematica
Clear[f,g]
g[x_] := x^2
f[x_] = D[g[x],x]
```

它之所以有效，是因为 `Set`（`=`） 不像 `SetDelayed` （`:=`）那样具有 `HoldAll` 属性而只是具有 `HoldFirst` ，以至于等号右边的 `D[g[x],x]` 被计算为 `2x` ，之后 `Set` 才把符号 `x` 视为一个模式名并定义 `f[x_] :> 2x` 。这相当于用户写 `f[x_] := 2x` ，于是结果当然是正确的。

顺便一提，由于这里我们所关注的 `Set` 和 `SetDelayed` 的区别仅仅在于“计算等号右边的表达式是在定义之前还是定义之后”，我们完全可以用另一种方法促使 `D[g[x],x]` 被计算：

```mathematica
Clear[f,g]
g[x_] := x^2
(f[x_] := #)&@D[g[x],x]
f[x_] := Evaluate@D[g[x],x]
```

## 设置 Simplify/FullSimplify 的 TransformationFunction 选项和 ComplexityFunciton 选项

`Simplify` 会不断地对原始表达式进行代数变换，并使用一个复杂度函数去测试这些变换结果，并返回所有结果中复杂度最低的那个。`FullSimplify` 则默认会尝试更多种类的代数变换。通过调整允许的变换类型和复杂度函数，我们可以控制 `Simplify` 的行为。

我现在以贴吧讨论贴 https://tieba.baidu.com/p/6771014401 中楼主所陈述的需求为例展示这一做法：

`Integrate[E^(-x), {x, 0, t}]` 输出了 `1 - Cosh[t] + Sinh[t]` ，但他不希望结果是由双曲函数表示的，因为 `1-E^(-t)` 这个结果看起来更简单，但即便主动使用 `Simplify` 乃至 `FullSimplify` 也得不到这个结果。

`Simplify` 无法将双曲函数转化为指数函数的一个根本原因是 `Simplify` 所尝试进行的代数变换中根本就不包括这一转化。令 `TransformationFunction ` 选项包括 `TrigToExp` 就可以解决这一问题：

```mathematica
Simplify[Integrate[E^(-x), {x, 0, t}], TransformationFunctions -> {Automatic,TrigToExp}]
```

或者使用 `TransformationFunction` 默认就包括 `TrigToExp` 的 `FullSimplify` ：

```mathematica
FullSimplify[Integrate[E^(-x), {x, 0, t}]]
```

不过，以上两条指令都给不出 `1-E^(-t)` 的结果。这是因为 `Simplify`/`FullSimplify` 默认的复杂度函数并不认为 `1-E^(-t)`  比 `1 - Cosh[t] + Sinh[t]`  更简单，你可以自己指定 `ComplexityFunction` 选项来改变这一状况：

```mathematica
Simplify[Integrate[E^(-x), {x, 0, t}], TransformationFunctions -> {Automatic,TrigToExp}, ComplexityFunction -> (If[FreeQ[Cosh|Sinh]@#,LeafCount@#,10000]&)]
```

```mathematica
FullSimplify[Integrate[E^(-x),{x,0,t}], ComplexityFunction -> StringLength@*ToString]
```

## 指定假设、局部假设和全局假设

在许多内置的符号计算函数中，都允许用户指定假设。

```mathematica
Refine[Sqrt[n^2], n>0]
Integrate[x^n, {x, 0, 1}, Assumptions -> n>0]
```

也可以不必分别为每个符号计算函数指定假设，而只需使用 `Assuming` 在一个范围内指定局部假设：

```mathematica
Assuming[n>0, Refine@Sqrt[n^2] + Integrate[x^n, {x, 0, 1}]]
```

或者直接指定全局假设

```mathematica
$Assumptions = n>0;
Refine@Sqrt[n^2] + Integrate[x^n, {x, 0, 1}]
```

帮助文档中已提到，使用 `Assuming` 实际上就相当于使用 `Block` 赋予 `$Assumptions` 以局部定义：

```mathematica
Block[{$Assumptions = $Assumptions && n>0}
	Refine@Sqrt[n^2] + Integrate[x^n, {x, 0, 1}]
]
```

## 使用 Quantity 表示物理量

`Quantity, QuantityVariable` 相关的功能从文档中可以了解到基本的用法。不过有两个技巧我认为值得介绍。

### 1

原本 `Quantity` 表达式可以借助“自由格式”（快捷键 `Ctrl` + `=` ）快捷输入，但由于自由格式输入的判断分支较多且可能调用云计算功能，所以在国际网络线路不好的地方输入效率不尽人意。通过执行以下代码，可以设定 `InputAlias` ，使得你可以像输入希腊字母（如 `Esc` alpha `Esc` ）那样使用 `Esc` qu `Esc` 来辅助输入 `Quantity` 表达式。

```mathematica
SetOptions[$FrontEndSession, InputAliases -> {
	"qu" -> TagBox[GridBox[{{"\[Placeholder]","\[Placeholder]"}}],"Quantity"]
}];
MakeExpression[TagBox[GridBox[{{v_, u_}}], "Quantity"], StandardForm] := HoldComplete@Quantity[ToExpression@v, u]
```

![效果展示](Figures/QuantityInputAlias.png)

顺便一提，`Esc + qu + Esc` 生成的两个占位符可以通过 `Tab` 键和 `Shift+Tab` 键快捷定位。

如果希望这个设定是永久的，你可以将上述代码中的 `$FrontEndSession` 改为 `$FrontEnd` 。

我再提供一个基于 `TemplateBox` 的版本：

```mathematica
SetOptions[$FrontEndSession, InputAliases -> {
	"qt" -> TemplateBox[
		{
			"\[SelectionPlaceholder]",
			"\[Placeholder]"
		},
		"QuantityTemplate",
		InterpretationFunction -> Function@TemplateBox[{#1, "", "", #2 /. s_String :> MakeBoxes@s}, "Quantity"],
		DisplayFunction -> Function@FrameBox[
			GridBox[{{#1, StyleBox[#2, LanguageCategory -> None]}}
				, GridBoxItemSize -> {"Columns" -> {{Full}}, "Rows" -> {{Full}}}
				, GridBoxSpacings -> {"Columns" -> {{1}}}
			]
		, RoundingRadius -> 2
		, StripOnInput -> False
		, FrameMargins -> 0
		],
		Tooltip -> Automatic
	]
}]
```

如果想要进一步提高方便程度，你可以为某个单位制的 `UnitConvert` 设定快捷键。至于怎么设快捷键，可以考虑简单的 `FrontEndEventAction` ，或者修改前端配置文件（可以看看 Mathematica Stack Exchange 上的贴子）。

### 2

对于数和量的乘法，数会自动结合到量的数值部分：

```mathematica
4 Quantity[4,"s"] //InputForm
```

```
> Quantity[16, "Seconds"]
```

但符号和量的乘法则不然

```mathematica
s*Quantity[4,"s"] //InputForm
```

```
> v*Quantity[4, "Seconds"]
```

这会导致同时包含有符号和量的大型表达式无法自动约简：

```mathematica
test = Quantity[1/5,"Kilometers"]/c + Quantity[-1/5,"Kilometers"]/c
```

![未自动约简的表达式](Figures/MixedQuantity1.png)

要使符号参与量的运算，有两种思路：

1. 将符号转化为量
2. 自己替换

具体方法这里不详细介绍，仅仅是启发一下读者。第一种做法类似于：

```mathematica
test /. c -> Quantity[c, IndependentUnit["Non-dimension"]]
% /. IndependentUnit["Non-dimension"] -> 1
```

![创建一个无量纲单位](Figures/MixedQuantity2.png)

第二种则类似于：

```mathematica
test //.(h:Plus|Times)[a___?(FreeQ[_Quantity]),Quantity[v_,u_],c___?(FreeQ[_Quantity])]:>Quantity[h[a,v,c],u]
```

![手动替换](Figures/MixedQuantity3.png)

## 将你的符号计算步骤分离为表示、计算、渲染三个部分

抽象地说，这一做法主要涉及了三个技术要素：

1. 自己定义用于表示抽象数学表达式的对象，而不是使用内置的函数。另一种做法是使用 `Inactivate` 系列功能来避免表达式被计算，以此保持表达式的抽象性。
2. 这些表示数学表达式的 Wolfram 语言表达式本身并不具有任何数学意义，仅仅是用于模式匹配、替换。仅当用户主动要求它们被计算时，它们才发挥出数学意义。
3. 使用 `Format` 或 `MakeBoxes` `MakeExpression` 或者 ``` Notation` ``` 程序包自己定义输入/输出显示规则

> 如果你没有掌握这些技术，想必你一定是看不懂上面三条说的是什么的。接下来我提供两个案例并解说。

### 1

第一个案例是源于一位知乎用户的求助。

 **<u>需求</u>** ：众所周知，在和式 $ \sum_{i=1}^nf(i) $ 中，指标 $ i $ 区分了不同的项但是不同的指标并不对应着不同的和式，这样的指标称为 **哑指标** （Dummy Index）。现在有 $ \sum_{i=1}^5f(i)+\sum_{j=1}^5g(j) $ ，两项和式采用了不同的哑指标，我们想要<u>将哑指标统一</u>，进而方便<u>合并同类项</u>。

 **<u>思路一</u>** ：利用内置函数 Sum 表示和式，将上述和求出来，构成一个具体的形如 $ f(1)+f(2)+g(1)+\dots $ 的表达式——它不包含任何求和符号 $ \sum $ 。这样一来哑指标当然就不存在了，我们也就可以不受哑指标影响地合并同类项。这一合并操作的实现可以参考本回答第一部分的案例。

 **<u>思路二</u>** ：上述做法有严重缺陷，它绕过了哑指标的统一，而是直接将这个和求出来再进行我们想要的表达式操作。很多情况我们需要对抽象的和式比如 $ \sum_{i=m}^n f(i) $ 进行操作，如果事先不指定参量 $ m,n $ ，内置函数 Sum 不可能完成这一求和。现在我将定义一个表示抽象和式的 SumHeld 对象，以稍完善些的方式来解决这一问题：

```mathematica
(* 定义 SumHeld 表达式如何显示 *)
SumHeld /: MakeBoxes[SumHeld[expr_, ranges__], form_] := MakeBoxes[Sum[expr, ranges], form]

(* 提示 SumHeld 表达式如何输入 *)
SumHeld /: SyntaxInformation[SumHeld] = {"LocalVariables" -> {"Table", {2, Infinity}}};

(* 定义 IndexUnify 函数来使 SumHeld 对象所构成的和具有尽可能统一的哑指标 *)
IndexUnify[HoldPattern@Plus[sums:SumHeld[_, __]..]] := Plus@@With[
	{
		targetIndices = List@@#[[-1, 2;;, 1]],
		sourceIndicesList = List@@@#[[;;, 2;;, 1]]
	},
	Function[{sum, sourceIndices},
		sum /. Thread[sourceIndices -> Take[targetIndices, Length@sourceIndices]]
	]@@@Transpose@{#, sourceIndicesList}
]&@SortBy[Flatten/@{sums}, Length]

(* 定义 SumTogether 函数来合并和式 *)
SumTogether[HoldPattern@Plus[sums:SumHeld[_, sameRanges__]..]] := SumHeld[Plus@@{sums}[[;;, 1]], sameRanges]
SumTogether[HoldPattern@Plus[sums:SumHeld[_, __]..]] /; UnsameQ@@{sums}[[;;, 2;;]] := Plus @@ SumTogether@*Plus @@@ GatherBy[{sums}, Rest]
```

接下来我们用 $$ \sum _{j=1}^5 \left(\sum _{b=1}^5 g(b,j)\right)+\sum _{a=1}^5 \sum_{i=1}^5 f(a,i) $$ 测试上述功能

```mathematica
(* 注意以下两项和式是以不同的方式输入的，两种方式实际上可以使用 Flatten 和 Fold 相互转化 *)
test = SumHeld[f[a, i], {a, 1, 5}, {i, 1, 5}] + SumHeld[SumHeld[g[b, j], {b, 1, 5}], {j, 1, 5}]
% //IndexUnify 
% //SumTogether
```
![效果图](Figures/v2-9552a417bc69a060d9da56886af9cd54_r-8e18c1d0-f1aa-45f7-93ea-5cfcf0af15b9.jpg)

这只是个原型，可改进的地方还很多，比如可以规定涉及无穷项求和的情况不执行和式的合并。再就是这个输出其实还能再好看一些

![要是还不满足就用 MaTeX 包去调 TeX 排版公式吧](Figures/v2-b0f678d0b11b6fdbcc2c7cf6e378ebb9_r-87cf271e-0946-480d-a668-0d293167f638.jpg)

### 2

第二个案例是我一个计算项目的一部分，一些数学/物理知识可能会对理解它起到显著的作用。

现在有一个由狄拉克记号表示的内积：$\langle\psi_{H_{1s}}(r_1)|\psi_{H_{1s}}(r_2)\rangle$，我们可以用 Wolfram 语言这样表示：

```mathematica
(* 狄拉克 Braket 记号 *)
BraKet[{ψH1s[r1]}, {ψH1s[r2]}] //TraditionalForm

(* 因为之后要给内部的符号赋值，所以这里预先设一个 HoldAll 属性来保持 Braket 表达式的抽象性（美观性）。 *)
SetAttributes[BraKet, HoldAll];

(* 为了美观，定义输出样式为下标样式 *)
ψH1s /: MakeBoxes[ψH1s, TraditionalForm] := FormBox[SubscriptBox["ψ",SubscriptBox["H", "1s"]], TraditionalForm]
```

![一定字体的TraditionalForm下，Braket的渲染结果](Figures/BraKet.png)

`Braket` 中的两个符号实际上表示的是氢原子基态的态矢量，它们在坐标表象下就是两个函数（即波函数）

```mathematica
(* 氢原子基态波函数的定义 *)
ψH1s[r_] := 1/Sqrt[π a0^3] Exp[-r/a0]

(* 在TraditionalForm下，渲染a0为下标式 *)
a0 /: MakeBoxes[a0, TraditionalForm] := FormBox[SubscriptBox["a","0"], TraditionalForm]
```

`Braket` 所代表的内积在坐标表象下就变为一个积分，现在我们要计算这个积分。

```mathematica
(* Integrate内部的细节不重要，读者只需要知道是算 bra*ket 的积分就行了。具体大概就是把两个函数乘起来然后作个坐标变换再积分。 *)
Calculate@BraKet[{bra_}, {ket_}] :=  Integrate[
	Conjugate[bra]*ket //someTransform
, {\[Mu],1,\[Infinity]}, {\[Nu],-1,1}, {\[CurlyPhi],0,2\[Pi]}]

(* 我当时所采用的坐标变换（从球坐标到长球面坐标系），对读者而言不重要 *)
someTransform[expr_] := expr*(R^3 (μ^2-ν^2))/8 /. Thread[{r1, r2} -> {(μ+ν)R/2, (μ-ν)R/2 }] //Simplify (* 也可使用 TransformedField *)
```

注意到我这里没有定义 `BraKet[{bra_}, {ket_}] := Integrate[...]` 而是定义了 `Calculate@BraKet[{bra_}, {ket_}] := Integrate[...]` 。这是因为我们往往既需要抽象的 `Braket` 表达式，又有时会想要把它所代表的那个内积计算出来。

为了方便，我们还可以定义

```mathematica
CalculateAll["InnerProduct"][expr_] := expr //. braket_BraKet :> Calculate@braket
```

来计算一个表达式所包含的所有内积。

以类似的思路，我们也可以自由地指定何时为那些物理常数代入具体数值：

```mathematica
Calculate[const:e|a0] := <|
	e -> Quantity[1,"ElementaryCharge"],
	a0 -> Quantity[1,"BohrRadius"]
|>@const

CalculateAll["PhysicalConstant"][expr_] := expr //. const:e|a0 :> Calculate@const
```

### 3

本节的导言中提及的一些技术还未被解说，读者可以自行探索。

---

[Mathematica Stack Exchange - Advice for Mathematica as Mathematician's Aid](https://mathematica.stackexchange.com/questions/92686/advice-for-mathematica-as-mathematicians-aid/92689)

---

能说的其实很多，文中每一部分都可以展开讲一大堆，这里的介绍只是姑且作个引子，让读者明白“这样的问题是可解决的”并了解“解决方案大概就在几个关键词附近”即可。据我观察，做理论推导者的 Mathematica 知识体系实际上通常是比较“畸形”的：他们可能对自己领域中的内置函数、程序包用法非常熟悉，但往往却缺乏最基本的 Wolfram 语法知识。所以我认为本文也不适合写得系统，故只是借助一系列案例来解说。