# 用 Wolfram Language 的区域计算功能绘制隐函数图像

> 此文源于问题 [怎么mathematica做隐函数方程组的图呢？](https://www.zhihu.com/question/265140400) 。

---

## 绘制嵌入在三维区域中的一维区域的三维图

首先把隐函数方程表示为相应维度的区域：

```mathematica
reg3d = ImplicitRegion[x^2+y^2+z^2==1 && x+y+z==1, {x,y,z}];
```

这是一个嵌入在三维空间中的一维区域，只有一个自由变量。

```mathematica
{RegionDimension, RegionEmbeddingDimension}@reg3d //Through
(* Out = {1,3} *)
```

当然，这个区域是比较简单的，它还有另一个更直观的表示法：

```mathematica
RegionIntersection[
	InfinitePlane@{{1,0,0},{0,1,0},{0,0,1}},
	Sphere[]
]
(* Out = BooleanRegion[#1 && #2 &, {InfinitePlane[{{1, 0, 0}, {0, 1, 0}, {0, 0,
1}}], Sphere[{0, 0, 0}]}]*)
```

如果你使用的是 Mathematica 笔记本环境，那么只需

```mathematica
Region@reg3d
```

就能将不高于的三维区域直接绘制出来。如果你需要绘制 $x,y$ 之间的函数关系，那么只需使用

```mathematica
Region[reg3d, ViewPoint -> {0,0,1}]
```

。

如果你不处于笔记本环境，可以使用

```mathematica
Export[
	path,
	Show@Region[reg3d, ViewPoint -> {0,0,1}]
]
```

看图。

## 是否能够降低维度到二维？

如果我们期望得到的是 $x,y$ 之间的函数关系，那么我们自然希望得到二维图，而之前绘制的是三维的。

只需做一个投影变换

```mathematica
reg2d = TransformedRegion[
	reg3d,
	TransformationFunction[{
		{1, 0, 0, 0},
		{0, 1, 0, 0},
		{0, 0, 0, 1}
	}]
]
(* Out = ParametricRegion[{{x, y}, x^2 + y^2 + z^2 == 1 && x + y + z == 1}, {x, y, z}] *)

{RegionDimension, RegionEmbeddingDimension}@reg2d //Through
(* Out = {1, 2} *)
```

它返回的结果表明，实际上你一开始就可以使用 `ParametricRegion` 来在二维空间中表示一个三元方程所确定的区域！

如果你只希望投影原来的区域，而不想投影整个空间而降低所嵌入空间的维数，可以使用：

```mathematica
TransformedRegion[
	reg3d,
	ScalingTransform@{1,1,0}
]
```

要延伸到全空间则可使用

```mathematica
RegionProduct[reg2d, FullRegion@1]
```

## 如何提取离散数据点用于自己绘图？

以上的区域计算都是解析的，能约化的内核都已约化，不能约化的也不会以丢失精度为前提去约化而是保持不计算。而绘图时必然要采样数据点，我们是否可以方便地使用它的采样结果？

你可以直接使用 `DiscretizeRegion` 将区域离散化，它将返回 `MeshRegion` 对象（ `MeshRegion` 的表示法类似于 `GraphicsComplex` ）：

```mathematica
DiscretizeRegion@reg2d
(* Out = MeshRegion[{一堆点坐标, {Line[一堆端点编号]}] *)
```

这个对象是原子的，要提取其中的点坐标和图形基元，只能分别使用 `MeshCoordinates` 和 `MeshCells` 。

下面这个例子会把一个三维空间中的区域离散化，然后提取点集并去除第三维坐标、提取线段基元，从而可以用 `Graphics` 绘制二维图：

```mathematica
DiscretizeRegion@reg3d;
Graphics@GraphicsComplex[
	Most /@ MeshCoordinates@%,
	FirstCase[MeshCells@%, {__Line}]
]
```

实际上，这样得到的结果与

```mathematica
Show@Region@reg2d
```

给出的 `Graphics` 对象基本相同。

