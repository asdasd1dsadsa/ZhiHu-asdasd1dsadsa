# WolframLanguage作为一门优秀的语言为什么没有被普及？价格是主要的原因吗？

注意：此时申请许可证的网页已重新开放，各位备好梯子（最好是美国的，不然还是慢）申请吧。

---

偏个题——但是我感觉值得普及一下了：现在Wolfram Language已经推出 **免费** 的Wolfram引擎作为独立的解释器\(安装包大小约1G\)，支持Windows、Linux、Mac系统。

## Wolfram引擎

详细介绍可以参考

[Free Wolfram Engine for Developers](https://www.wolfram.com/engine/)

这里我简单总结一下上文的内容：

1.  **作为解释器** ：你可以用这个引擎来执行你的脚本，也可以在C、Python语言里直接调用这个引擎来执行命令。如果你使用的语言目前没有现成的接口支持，你也可以自己用sockets 和 ZeroMQ来连接这个引擎。
2.  **附送云服务** ：它还附赠一个Wolfram云服务的基本版许可证，从而你可以在云端执行命令。
3.  **许可** ：允许个人和企业级开发，分发给用户使用需要许可（除非用户也是开发者）。开发不受限制，软件产品分发需要许可。服务演示不受限制，服务收费不被允许。教育用途需要教育许可证，这可以由所在学校的站点许可证来提供。

在

[Wolfram Engine for Developers License](https://wolfram.com/developer-license)

可以为你的Wolfram ID申请到Wolfram Engine的许可证，用Wolfram ID即可激活你所下载安装的Engine。

## 前端

结合 **Jupyter Notebook** 可以实现类似Mathematica的操作体验，帮助文档可以看Wolfram官网上的。Engine的下载和Jupyter Notebook的配置参考下文

[jane lia：给Anaconda的Jupyter notebook笔记本安装Wolfram engine核心](https://zhuanlan.zhihu.com/p/79730978)

除了JupyterNotebook，使用编辑器的插件来辅助写代码也是不错的选择。我现在使用VSCode，装了以下插件：

* Code Runner
* [Wolfram Language](https://zhuanlan.zhihu.com/p/52722078)
* [Wolfram Language Server](https://zhuanlan.zhihu.com/p/74921013)

基本已经够用了，感觉也很不错。我甚至感觉 Mathematica 笔记本 Default 样式表的折行、缩进体验不如带插件的纯文本编辑器。

关于VSCode相关配置，可参考

[asdasd1dsadsa：为Wolfram Engine配置VSCode（Windows）](https://zhuanlan.zhihu.com/p/102176115)
![](Figures/v2-1992ae14fadee716ac595ce4c58609ac_r-9d4edb27-29e7-48d6-bc8a-4daccb4e4f60.jpg)
---

现在Wolfram中国已经在淘宝开店 **，** MMA12\.0 **永久版学生560** ， **家庭版2500** 。如果用花呗 **分三期付，每月200/800** 对于条件尚可的学生/工作者应该都是可以承受的价格，穷学生的话大不了分12期，每月50。对于绝大多数人，MMA12\.0的功能都已经很够用，不必担心买永久版以后又更新换代多花钱。

[Wolfram Mathematica12.0 全球现代技术计算的终极系统](https://item.taobao.com/item.htm?spm=a230r.1.14.17.68e426b5HaQVYJ&id=607126029158&ns=1&abbucket=20#detail)


