# 如何学习和使用Mathematica？

此回答已于2020/11/14重写。

---

## 什么是 Mathematica ？

Mathematica 这款软件产品包括内核与前端两个部分，内核负责执行用户输入的指令，前端则负责与用户交互，把用户的输入传递给内核、把内核的计算结果渲染给用户看。学习 Mathematica 主要说的是学习输入指令给内核所用的 Wolfram 语言，前端的部分则通常不需要怎么认真学。

Wolfram 语言的独特优势是极高的抽象性和丰富的内置功能（而且其中大部分功能同时保持足够的抽象性），从而非常适合于符号计算这样的不局限于数值的应用场景。至于这种优势的具体案例，我不想再写了，浅显的例子在 比特之理 [[1]](http://www.kylen314.com/archives/5814) 和 知乎 [[2]](https://www.zhihu.com/question/27834147/answer/38337425) 的文章中已经举了很多了，而理解深入的例子又对读者的知识有要求。

总之，我对 Wolfram 语言的定位是“速写语言”。如果你不想关心堆栈内存的二进制数据，只想把全部的精力放在这些0和1所表示的对象上（不管是整数还是多项式中的变元还是一段聊天记录字符串）；或者如果你常常想要在尽可能短的时间内用程序验证一下你突然想到的的点子是否可行，而不想要体会付出很多精力编程最后却发现它并没有价值的失落感觉，那么这门语言就是非常正确的选择。

> 当然，也有很多人只是因为如 FeynCalc 这样的工具箱要求你使用 Wolfram 语言而没有别的选择。

作为前端的 Mathematica 也是十分出色的产品，它允许你创建“可计算文档”，这是 Wolfram Research 的招牌之一，近来流行的 Python Jupyter Notebook 正是借鉴于 Mathematica Notebook 设计的，而直到如今 Mathematica Notebook 的功能仍远强于 Jupyter Notebook 。如果你不理解什么是可计算文档，你可以想象你写的一篇 (MS Office) Word 文档中有一条公式和这个公式所表示的函数的一幅曲线图，你在向他人展示它们的时候可能希望：在修改公式的同时，图也随之而变化。这就是可计算文档，是计算和排版需求的结合，这样的需求在 Mathematica Notebook 是十分轻而易举就能实现的。

## 如何学习 Wolfram 语言？

### 关于内置帮助文档

对于初学者，我建议一定要安装 Mathematica 而不要使用免费却没有前端的 Wolfram Engine ，因为 Mathematica 带有可能是全世界最好的帮助文档（而且是全中文的！）。这个帮助文档中既有对内置功能用法的概括性总结，也有大量示例——你可以直接在这些示例代码上作修改来验证你读文档时产生的理解是否正确。在笔记本里输入一段文字，选中它然后按F1即可打开帮助文档，挺方便的。

> 记得QQ群里有个人的群昵称叫：私聊不回，问就是F1。（笑）

Mathematica 的帮助文档是百科全书式的，每一篇文档都尽量使自己保持独立，然后有关的文档之间会相互链接。我认为这是最好的文档模式之一，但百科全书有一个影响重大的缺点：没能预先指出一条主线从而不够平易近人。所以，很多人可能会更倾向于使用教材这样的有主线的、更亲切的资料。实际上帮助文档包含一本叫“虚拟全书”的教材，我推荐阅读。

### 关于教材

就我本人而言，**我从未从教材上学到关于 Wolfram 语言的新知识** ，也就是说，单凭帮助文档而不读教材掌握它是没有原则上的困难的。如果你能够适应、熟悉如何利用这本百科全书，那就是最好的了。

> 如果你常在维基链接里点来点去，那你应该不会对这帮助文档有什么不适应的地方。

如果你觉得还是需要读教材，我推荐纯新手可尝试阅读官方的 [Wolfram 语言入门](https://www.wolfram.com/language/elementary-introduction/) （有中文实体书卖），稍熟悉一些了的用户可以转而阅读 Leonid Shifrin 的《Mathematica Programming Advanced Introduction》。我在此提供它的三个链接：

* [官网英文PDF](http://www.mathprogramming-intro.org/)

* [中文Notebook文件](https://gitee.com/asdasd1dsadsa/ZhiHu-asdasd1dsadsa/raw/master/Resources/Mathematica%20Programming%20Advanced%20Introduction.7z)

* [中文网页版（未全部翻译，但对新手而言足够了）](http://www.mathcraft.org/wiki/index.php?title=Mathematica_%E7%BC%96%E7%A8%8B%EF%BC%9A%E9%AB%98%E7%BA%A7%E5%AF%BC%E8%AE%BA)

再深入一些的话可以读 David B. Wagner 《Power Programming with Mathematica: THE KERNEL》。

> 说是深入，其实后两本书初学者也都是可以直接读的，而且层次也并不深于帮助文档。

这软件出现这么多年了，原创的中文好书真是没有几本，多数坑人。

> [某豆瓣书评提及：](https://book.douban.com/review/5371515/) “现在市面上各种讲解Mathematica的书很多，有各种老师出的，有各种教育界的一哥一姐出的，但总的来说，他们大都是很敬业很顽固的在抄Help文档。而且抄也不用心，tutorial中有那么多经典的概念不帮我们阐述一下，比如如何用函数式编程写出优美的程序、如何用模式匹配绽放出属于M语言的那朵奇葩。这些概念对于不搞计算机的是不那么容易理解的。而这些是他们根本就不考虑的，他们大都上来就搞计算，搞数值分析，搞画图，搞这搞那，有没有搞错，Mathematica根本就不是一个大号的计算器，它是一种编程语言！说到编程，更是恼火，各种书里充斥着大片的for循环，if判断，而且搞得有腔有调，你当你在玩C++啊。Mathematica发布都24年了，我们的书籍还停留在这个水平，我对此还能说啥？”
>
> [某贴吧精品提及：](https://tieba.baidu.com/p/3452773595)“总而言之，那时的我对**Mathematica其实是一种严谨的编程语言**这点可以说是完全没有认识，而那本**万恶的**《Mathematica7实用教程》也基本上只是把Mathematica当成一个计算器在介绍，这便进一步封死了我认知自己错误的路。参赛的结果，也就不消我多说了吧。这次失败让我陷入了强烈的沮丧之中，从此一厥不振，Mathematica成为了我心中又一段不堪回首的记忆——要真是这样我现在就不会在这儿了，故事当然还有后续。”

### 练习

做些练习对提升水平是很有帮助的。比较适合 Wofram 语言的习题有：

* Project Euler (推荐参考 [Aster](https://github.com/GalAster) 的题解：[euler.ea.chat]())
* [官方题库](https://challenges.wolfram.com/)

### 帮助文档和教材所不会告诉你的

* 讨论 Wolfram 话题的最大的第三方平台是 Mathematica Stack Exchange 。MSE 和官网的社区水平远超你在国内平台看到的任何地方。其他不能在帮助文档和教材上找到的，基本上都可从这里得知。
* 中文资料主要聚集于百度贴吧，贴吧精品贴总结并详细解释了许多新手常犯的错误，知乎也有一些经验贴。果壳和豆瓣上的社区似乎已经不复存在。
* 问问题一般去各讨论群、百度贴吧、[MMA问答网](mmaqa.com) 
* 淘宝等地好像有相关技术服务，有钱能使鬼推磨，让磨推我也是可以的（笑）
* Wolfram Research 日子过得不咋好，大家多支持正版。淘宝店学生版 560 块。
* 你专业的具体应用所需的程序包可能有人写过，上官方 Library 搜搜也许有惊喜，不过说到底这种事还是应该拿去问你专业内的 WL 使用者。

---

## Wolfram 语言水平评级

如果你决定使用帮助文档来学习 Wolfram 语言，你可参考此评级标准来区分知识的“层次深浅”。上述的介绍主要是帮助读者达到 Level 0 。这份评级则通过提供眼界帮助读者树立正确的学习方向。

> 强烈建议新手阅读贴吧精品贴[《学习Mathematica时最常见的问题》](https://tieba.baidu.com/p/2964416898)

### Level 0

掌握基本语法，具备继续学习的潜力

* 写代码基本不会出现语法错误（SyntaxError）
* 能够迅速借助帮助文档学会某陌生内置函数的用法
* 知道如何定义函数
* 知道函数的选项是什么样的

> Level 0 -> Level 1寄语
>
> * Wolfram 语言中一切都是表达式。
> * 在 Wolfram 语言中，如果你正使用 For 循环来做一件事，那么你往往选择了错误的方式。
> * 多数情况的性能问题是使用不当导致的。内置功能也大多是经过大量优化的二进制程序，它们一般比未经优化的 C 代码还快得多。

### Level 1

大致掌握核心语法，掌握核心模块中的核心功能

* 核心语法
	* 表达式
		* 表达式：原子表达式、一般表达式、头部、子表达式、层、位置、表达式序列
		* 符号：符号、上下文、定义、直接值（OwnValues）、下值、子值、属性
			* Flat Orderless
			* Set SetDelayed
			* Clear ClearAll ClearAttributes
		* 字符串和数的基本输入方式
	* 表达式的计算
		* HoldAll, HoldFirst, HoldRest 属性
		* 无穷替换
	* 广义运算符（`f@`,`~f~`,`//f`）
* 核心功能
	* 原子类型
		* 符号：Symbol SymbolName Context
		* 字符串：ToString ToExpression FromCharacterCode ToCharacterCode
		* 数：Rationalize IntegerDigits IntegerString FromDigits Re Im Abs Arg
	* 数据结构
		* Rule Association
			* Keys Values Query
	* 列表生成
		* Table Range Array Tuples Permutations
	* 表达式操作
		* 基于位置：Part Delete Insert ReplacePart RotateRight Reverse
		* 基于长度：Partition PadRight
		* 基于层：Level、Flatten
		* 基于元素：Select Pick
		* 基于集合：Union Intersection Complement
		* 基于关系：Sort SortBy Gather GatherBy Split SameTest
	* 规则、模式与替换
		* Pattern Blank BlankSequence BlankNullSequence Repeated RepeatedNull Alternatives Except PatternTest Condition PatternSequence
		* MatchQ Cases Replace ReplaceAll ReplaceRepeated
		* MemberQ Count Position
	* 逻辑
		* True False
		* Not And Or
		* Great Less SameQ UnsameQ
	* 控制
		* CompoundExpression If
		* Catch Throw Return
		* Reap Sow
		* With Block Module
		* ```$RecursionLimit``` ```$IterationLimit```
		* Do For While Break Continue
	* 函数式编程
		* 表示函数：Function Slot SlotSequence Composition
		* 应用函数于表达式特定内部：Apply Map MapAt MapThread MapIndexed Inner
		* 嵌套应用函数：Nest NestWhile Fold FixedPoint
	* 字符串处理
		* StringPart StringJoin StringRiffle StringTrim
		* 字符串模式：StringExpression StartOfString DigitCharacter 等
	* 数值
		* Indeterminate DirectedInfinity
		* N SetPrecision NumericQ MachinePrecision
		* WorkingPrecision AccuracyGoal
	* 文件操作
		* Import Export
* 强力功能
	* 前端
		* 动态：Dynamic DynamicModule Refresh
	* 随机数
		* 均匀随机数：RandomInteger RandomReal RandomPoint
		* 随机选择：RandomChoice RandomSample RandomPermutation
		* 随机数分布：RandomVariate
* 规范的程序开发
	* 上下文管理：Begin End BeginPackage EndPackage
	* 程序包文件：Get $UserBaseDirectory
	* 选项管理：OptionsPattern Options 
* 调试程序
	* InputForm FullForm
	* In Out Information
	* PrintTemporary Monitor
	* Abort Quit
	* 愿意看报错信息

> Level 1 -> Level 2 寄语
>
> * 更多的 F1 和帮助文档中链接的跳转点击
> * 更多地自己写代码测试一个功能的用法和细节行为
> * Mathematica 中的所有窗口都是一个笔记本。
> * Mathematica 笔记本是一个表达式。

### Level 2

大致掌握所有核心语法，对实用性较强的、支持程度较高的功能模块较熟悉。

* 核心语法
	* 表达式
		* 字符串和数的所有输入方式
		* 所有定义：上值、NValues、FormatValues、DefaultValues
			* UpSet TagSet
		* 知道定义会被自动排序
		* 属性：OneIdentity HoldAllComplete
	* 计算
		* 属性-上值-下值和子值对应的变换规则的执行顺序
		* HoldAllComplete对上值的影响
	
* 核心功能

	* 表达式操作
		* 帮助文档指南中提到的所有函数
	* 模式匹配
		* 保持模式：HoldPattern Verbatim
		* 序列匹配：SequenceCases OrderlessPatternSequence等
	* 控制
		* TimeConstrained MemoryConstrained
		* 非标准计算：Inactivate Activate Evaluate Unevaluated Hold ReleaseHold
	* 函数式编程
		* ListConvolve MovingMap
	* 指定表达式的输入输出格式
		* 输出：Format MakeBoxes StandardForm TraditionalForm OutputForm
		* 输入：InputForm MakeExpression 
	* 流
	* 链接（WSTP）
	* 数值
		* NHoldAll NumericFunction属性
	* 系统状态
		* SystemOptions
		* ClearSystemCache

* 强力功能

  * 图形图像
  	* Graphics Graphics3D GraphicsComplex Show GeometricTransformation
  		* 若干通用选项和封装（如 ImageSize ）
  	* Plot ListPlot ListPlot DensityPlot ComplexPlot RegionPlot 及其三维系列
  		* 若干通用选项和封装（如 Legended ）
  	* BarChart PieChart 等
  	* Image ImageData ImageTransformation ComponentMeasurements
  * 增强计算
  	* 编译
  	* 并行
  	* LibraryLink ExternalEvaluate
  	* 异步
  * 模版
  * 前端
  	* 各种菜单指令，尤其是：单元->显示表达式（Shift+Ctrl+E）和单元->转换成
  	* 表达式的四种基本形式：StandardForm TraditionalForm InputForm OutputForm（只有前两者是 `$BoxForms` ）
  	* 笔记本结构：Notebook Cell CellGroup BoxData
  	* 框符：RowBox GridBox TemplateBox InterpretationBox DynamicBox SubscriptBox等
  	* 框符的字符串表示
  	* 样式表：StyleDefinitions StyleData
  	* 笔记本操纵：NotebookGet SelectionMove
  	* 前端操纵：FrontEndTokenExecute FrontEndEventActions
  * Wolfram Cloud
  * 专业向功能（如数学、机器学习）和对其他程序提供的接口（如 ``` RLink` ```）
  * 计算机相关功能
  	* 文本、图像、音频、视频
  	* 网络
  		* SocketListen SocketConnect
  		* HTTPRequest URLRead
  		* GenerateHTTPResponse
  	* 数据交换
  		* WXF
  		* MX
  	* 设备控制
  		* DeviceOpen DeviceReadBuffer

* 重要的数据类型/结构

  * Association Dispatch
  * Graph
  * ByteArray SparseArray
  * Quantity Entity Dataset
  * Region MeshRegion

* 程序调试

	* Trace Stack
	* 自带调试器

* 程序包开发

	* Assert Message Check On Off Quiet
	* Needs DeclarePackage
	* Encode DumpSave
	* ``` GeneralUtilities` ```
	* ``` Package` ``` ``` PacletManager` ```

> Level 2 -> Level 3 寄语
>
> * 更多地阅读 MSE 上的精品贴，更多地自己测试 Wolfram 系统的行为。
> * 大量阅读内置程序包，并使用 ``` GeneralUtilities`PrintDefinitions ``` 了解通过 MX 文件载入的定义。

### Level 3

将 Wolfram 语言用于你专业领域的工作一般并不需要多少 Level 3 技能。掌握有 Level 3 技能的全部，并不意味着你在将在应用 Wolfram 语言的工作中将表现地更好，只说明你已经彻底了解了这个系统的设计，结合一定的计算机技术你就可以重新实现 Wolfram 系统。

我当然不具有 Level 3 水平，所以以下描述会是不完整的。

* 掌握核心语言特性及其完整行为
	* 定义规则的默认排序
		* ``` Internal`ComparePatterns ```
	* 无穷计算终止的判据
	* `Condition` 的特殊地位和使用技巧
		* Update
		* RuleCondition
	* ``` Internal` ``` ```Developer` ```
* 掌握 Wolfram 系统本身的状态带来的影响
	* 了解 Wolfram 系统的启动步骤和主循环
	* 知道数据是否会被共享、是否会被丢弃
	* 知道数据是会被复制还是修改
	* 知道表达式在初始化时占用的内存情况
* 掌握表达式如何送往内核进行计算
* 更多地、更细节地掌握内置功能模块，尤其是：
	* 流
	* 链接
	* 前端
* 了解核心语言特性和核函数的实现细节（Implementation Details），尤其是：
	* 模式匹配的基本原理
	* 序列匹配的方法原则，尤其是和字符串匹配的方法之间的差异
	* 自动编译
* 了解采用不同定义方式以及采用不同核函数会导致怎样的不同性能表现（Performance Tuning）

> 按我自己的评级方案评价自己，我大概是 LV1 100%, LV2 80%, LV3 5% 。这是对 Wolfram 语言及其运行环境的了解程度的打分，要论计算机技术水平我就一渣渣。

### 原来的评级方案

>  原来的评级方案，写得比较随意，比较具体而不抽象，你可以拿其中的问题问自己。
>
>  其他类似的能力评级的讨论在：[[1]](https://www.zhihu.com/question/55683496) [[2]](https://www.zhihu.com/question/35902513/answer/65724522)

Level1：懂一点基本的语法知识以及基本的操作。可能会自己造一堆轮子，但至少造得出来。

1. 核心概念：什么是表达式？什么是头部？什么是运算符？会不会用广义运算符（`f@`,`~f~`,`//f`）？什么是符号？什么是值？属性是什么？
2. 结构操作：教程里的列表操作函数会用多少了？知道用基本的规则与替换吗？字符串的基本操作和输入转义会吗？怎么在列表、规则、字符串、一般表达式之间转换？Prepend 和 PrependTo区别在哪？
3. 函数：如何自定义一个函数？有哪些方法？怎么把函数作用于表达式的内部？递归怎么写？
4. 控制（这个范围我设的有点广）：除了Table还会几种迭代？With、Block、Module分得清吗？Return返回到哪一层？
5. 其他内置符号体系：逻辑运算、逻辑判断函数熟悉了吗？Equal 和 SameQ 区别在哪？除了 Blank 模式你还知道用哪些模式？
6. 数学（不过，数学说到底功夫在语言本身之外）：数和数值有啥区别？公式化简除了 Simplify 还会啥？各数值计算功能的选项有会用的吗？微分方程会设边界条件吗？绘图功能都熟悉没？绘图选项会调吗？

Level2：各主要模块的帮助文档涉及的基本都会了，写码基本畅快，瓶颈在数学

1. 核心概念：计算顺序是怎么算的？常用运算符的优先级结合性都记得吗？什么是上下文？上值、下值、子值是什么？常见属性的作用懂了吗？
2. 结构操作：Tutorial里出现过的表达式结构操作、列表结构操作都会了吗？
3. 函数：Tutorial里的都会了吗？怎么（尽可能简单地）把Thread返回的结果变回去？
4. 控制：Accumulate 相当于 FoldList 什么？Hold,Evaluate,Inactive系列大概会用一点了吗？\$RecursionLimit这样普遍有效的控制参数知道几个？
5. 其他内置符号体系：子表达式序列匹配和子字符串匹配的匹配策略有什么根本不同？Interpreter的参数知道几个？Dynamic系列了解吗？Graph会用吗？
6. 数学：Simplify、NDSolve等数学函数是如何运作的？机器精度和任意精度下的数值计算有何区别？

Level3：堆功底了，需要钻研。对各模块的了解融会贯通（尤其是在编程中能够应用更多的数学），还hack了一些底层的玩意儿。

如何写面向对象范式的代码？字符串、数、符号以外的原子表达式（比如 Dispatch 、 SparseArray）知道多少？

Names\["System\`\*"\]里面的会多少？Names\["Developer\`\*"\]乃至Names\["Internal\`\*"\]呢？

这个那个究竟谁效率高（乃至语言核心函数的底层机制）？Dispatch 、 列表等是什么数据结构、在内存中是如何存储的？Compile 支持哪些内置符号、如何使用 LibraryLink、WSTP ？ToBoxes 和 MakeBoxes 的区别是什么？

程序包怎么写？这不只是说要会用BeginPackage，而是说要知道如何为用户创建服务，mma用户往往不是专业程序设计出身，缺乏系统的服务设计思想（呃，说的其实是我自己）。这其中的细节比如：如何处理各种各样的用户输入、选项；设计一个默认的、“Automatic”计算策略（而不是全部要求用户自己指定）；代码风格和异常处理；性能和可维护性的平衡……

不太冷门、接近语言核心的程序包有多少是你所熟悉的？要知道，许多你常用的功能原本是在一个程序包中的，后来才合并到System\`上下文中，要想要完全掌握相关内容，你应当了解该程序包本身。比如：你可能经常用到StringTemplate，但你可能没接触过Templating\`；你可能熟悉Cell和Notebook表达式，但你可能不了解FrontEnd\`的具体细节；还有 Interpreter 和 Interpreter\` 。

---

有必要一提的是，为了钻研，你往往必须学会：

1. 读程序包源码。可以利用FindFile和GeneralUtilities\`PrintDefinitions。
2. 将单元还原为单元表达式来看。可以使用Ctrl\+Shift\+E（Windows）。
3. 直接追踪计算过程。可以使用Trace（以及它的 TraceInternal 选项）。

当然，以上做法对核函数和前端内置函数都无能为力。夸张一点的话，你可以尝试反编译某些dll，不过这应该算是违法破解了，测试测试就得了。
